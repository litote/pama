/*
 *     PAMA
 *     Copyright (C) 2016  Litote
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as
 *     published by the Free Software Foundation, either version 3 of the
 *     License, or (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.litote.pama.crawl.cuisinelibre

import mu.KLogging
import org.jsoup.Jsoup
import org.jsoup.select.Elements
import org.litote.pama.data.json.ProductDao
import org.litote.pama.data.json.RecipeDao
import org.litote.pama.data.model.Id
import org.litote.pama.data.model.Product
import org.litote.pama.data.model.Recipe
import org.litote.pama.data.model.RecipeSource.CuisineLibre

fun main(args: Array<String>) {
    CuisineLibreCrawler.update()
    //
    //CuisineLibreCrawler.parseIngredient(ProductDao.findById(Id("434407e4-1092-4c00-ae96-5899c639a22f"))!!, mutableMapOf())
    //println(CuisineLibreCrawler.parseRecipeDescription("http://www.cuisine-libre.fr/chouchouka-de-blida"))
}

/**
 *
 */
object CuisineLibreCrawler : KLogging() {

    val rootUrl = "http://www.cuisine-libre.fr/"


    data class Description(val ingredientsScope: String,
                           val ingredients: List<String>,
                           val description: String)

    fun update() {
        val map = mutableMapOf<String, Recipe>()
        ProductDao.find().forEach { CuisineLibreCrawler.parseIngredient(it, map) }
        RecipeDao.save(map.values.toList())
    }

    fun parseIngredient(ingredient: Product, recipesMap: MutableMap<String, Recipe>): List<Recipe> {
        if (ingredient.cuisineLibreKeyword != null && ingredient.cuisineLibreKeyword != "none") {
            val doc = Jsoup.connect("$rootUrl${ingredient.cuisineLibreKeyword}/").timeout(10*1000).get()
            val elements = doc.select("li.clearfix > a")
            return elements.map {
                val href = it.attr("href")
                val hrefUrl = "$rootUrl$href"
                logger.info { href }
                var recipe = recipesMap.get(hrefUrl)
                if (recipe == null) {
                    val description = parseRecipeDescription(hrefUrl)
                    recipe = Recipe(setOf(ingredient._id),
                            CuisineLibre,
                            it.select("strong").text(),
                            hrefUrl,
                            null,
                            it.parent().select("em").last()?.text() ?: "",
                            description.ingredientsScope,
                            description.ingredients,
                            description.description)
                    Thread.sleep(1000L)
                } else {
                    recipe = recipe.copy(productsIds = recipe.productsIds + ingredient._id)
                }
                logger.info { recipe }
                recipesMap.put(hrefUrl, recipe)

                recipe!!
            }
        } else {
            return emptyList()
        }
    }

    fun parseRecipeDescription(url: String): Description {
        val doc = Jsoup.connect(url).timeout(10*1000).get()
        val ingredients = doc.select("#ingredients > h2").text()
        val ingredientsList = doc.select("li.ingredient").map { it.text() }
        val description = Elements(doc.select("div.instructions").first().children().filter { it.select("span > img").isEmpty() }).outerHtml().replace("class=\"manualbr\"", "").replace("class=\"spip\"", "")
        return Description(ingredients, ingredientsList, description)
    }
}