/*
 *     PAMA
 *     Copyright (C) 2016  Litote
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as
 *     published by the Free Software Foundation, either version 3 of the
 *     License, or (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.litote.pama

import io.vertx.core.AbstractVerticle
import io.vertx.core.http.HttpMethod
import io.vertx.ext.web.Router
import io.vertx.ext.web.handler.BodyHandler
import io.vertx.ext.web.handler.CookieHandler
import io.vertx.ext.web.handler.CorsHandler
import io.vertx.ext.web.handler.SessionHandler
import io.vertx.ext.web.sstore.LocalSessionStore
import mu.KotlinLogging
import org.litote.pama.data.json.CooperatorDao
import org.litote.pama.data.json.CooperatorParticipationDao
import org.litote.pama.data.json.DistributionDao
import org.litote.pama.data.json.ProductDao
import org.litote.pama.data.json.RecipeDao
import org.litote.pama.data.model.Cooperator
import org.litote.pama.data.model.CooperatorParticipation
import org.litote.pama.data.model.Distribution
import org.litote.pama.data.model.DistributionAttendance
import org.litote.pama.data.model.DistributionRole
import org.litote.pama.data.model.DistributionType
import org.litote.pama.data.model.Id
import org.litote.pama.data.model.Product
import org.litote.pama.data.model.Recipe
import org.litote.pama.data.model.alert.AlertType
import org.litote.pama.data.rest.DistributionCalendar
import org.litote.pama.data.rest.DistributionDetails
import org.litote.pama.data.rest.ProductInfo
import org.litote.pama.data.rest.RecipesRequest
import org.litote.pama.data.rest.RecipesResponse
import org.litote.pama.data.rest.alert.SaveAlertRequest
import org.litote.pama.data.rest.alert.TestAlertRequest
import org.litote.pama.data.rest.auth.AuthenticateRequest
import org.litote.pama.data.rest.auth.AuthenticateResponse
import org.litote.pama.data.rest.auth.ChangePasswordRequest
import org.litote.pama.data.rest.auth.ChangePasswordResponse
import org.litote.pama.data.rest.auth.LogoutRequest
import org.litote.pama.data.rest.auth.PasswordReminderRequest
import org.litote.pama.data.rest.auth.PasswordReminderResponse
import org.litote.pama.data.rest.auth.SessionRequest
import org.litote.pama.data.rest.auth.SessionResponse
import org.litote.pama.data.rest.cooperator.SaveCooperatorRequest
import org.litote.pama.jackson.JacksonMapperProvider.mapper
import org.litote.pama.service.AlertService
import org.litote.pama.service.AuthenticationService
import org.litote.pama.service.CooperatorService
import org.litote.pama.service.DistributionService
import org.litote.pama.util.Env
import java.nio.file.Files
import java.nio.file.Paths
import java.util.EnumSet

/**
 *
 */
class PamaServerVerticle : AbstractVerticle() {

    private val logger = KotlinLogging.logger {}

    override fun start() {
        val server = vertx.createHttpServer()
        val router = Router.router(vertx)

        //TODO server authentification

        if (Env.cors) {
            router.route().handler(corsHandler());
        }
        router.route().handler(bodyHandler());
        router.route().handler(CookieHandler.create());
        router.route().handler(SessionHandler.create(LocalSessionStore.create(vertx)))

        router.get("/rest/admin/login").handler { it.response().end() }

        router.get("/rest/admin/cooperators")
                .blockingHandler({ it.response().end(mapper.writeValueAsString(CooperatorDao.find())) }, false)

        router.get("/rest/admin/cooperator/:id")
                .blockingHandler({ it.response().end(mapper.writeValueAsString(CooperatorDao.findById(Id(it.request().getParam("id"))))) }, false)

        router.post("/rest/admin/cooperator")
                .blockingHandler({
                    val body = it.bodyAsString
                    val response = CooperatorService.saveCooperatorByAdmin(mapper.readValue(body, SaveCooperatorRequest::class.java))
                    it.response().end(mapper.writeValueAsString(response))
                }, false)

        router.post("/rest/cooperator/:id")
                .blockingHandler({
                    val body = it.bodyAsString
                    val request = mapper.readValue(body, SaveCooperatorRequest::class.java)
                    val old = CooperatorDao.findById(Id(it.request().getParam("id")))
                    //TODO auth check
                    if (old?._id != request.cooperator._id) {
                        it.fail(500)
                    } else {
                        val response = CooperatorService.saveCooperator(request, old)
                        it.response().end(mapper.writeValueAsString(response))
                    }
                }, false)

        router.post("/rest/admin/cooperator")
                .blockingHandler({
                    val body = it.bodyAsString
                    val response = CooperatorService.saveCooperatorByAdmin(mapper.readValue(body, SaveCooperatorRequest::class.java))
                    it.response().end(mapper.writeValueAsString(response))
                }, false)

        router.delete("/rest/admin/cooperator/:id")
                .blockingHandler({
                    CooperatorDao.deleteById(Id(it.request().getParam("id")))
                    it.response().end("{}")
                }, false)

        router.post("/rest/admin/cooperator/requestPasswordCreation")
                .blockingHandler({
                    val body = it.bodyAsString
                    val cooperator = mapper.readValue(body, Cooperator::class.java)
                    val result = AuthenticationService.requestPasswordCreation(cooperator)
                    CooperatorDao.save(cooperator)
                    it.response().end("{\"success\":$result}")
                }, false)

        router.get("/rest/admin/distribution/:id")
                .blockingHandler({ it.response().end(mapper.writeValueAsString(DistributionDao.findById(Id(it.request().getParam("id"))))) }, false)

        router.post("/rest/admin/distribution")
                .blockingHandler({
                    val body = it.bodyAsString
                    val distribution = mapper.readValue(body, Distribution::class.java)
                    DistributionDao.save(distribution)
                    it.response().end()
                }, false)

        router.delete("/rest/admin/distribution/:id")
                .blockingHandler({
                    DistributionDao.deleteById(Id(it.request().getParam("id")))
                    it.response().end("{}")
                }, false)

        router.post("/rest/admin/product")
                .blockingHandler({
                    val body = it.bodyAsString
                    val product = mapper.readValue(body, ProductInfo::class.java).toProduct()
                    ProductDao.save(product)
                    it.response().end(mapper.writeValueAsString(product))
                }, false)

        router.delete("/rest/admin/product/:id")
                .blockingHandler({
                    val product = ProductDao.findById(Id(it.request().getParam("id")))!!.copy(deleted = true)
                    ProductDao.save(product)
                    it.response().end("{}")
                }, false)

        router.get("/rest/admin/product/:id")
                .blockingHandler({
                    it.response().end(mapper.writeValueAsString(ProductInfo(ProductDao.findById(Id(it.request().getParam("id")))!!)))
                }, false)

        router.get("/rest/admin/products")
                .blockingHandler({
                    it.response().end(mapper.writeValueAsString(ProductDao.find().filter { !it.deleted }.map { ProductInfo(it) }))
                }, false)

        router.post("/rest/admin/product/uploadImage/:id")
                .blockingHandler({
                    val product = ProductDao.findById(Id(it.request().getParam("id")))!!
                    val upload = it.fileUploads().first()
                    val path = Paths.get(upload.uploadedFileName())
                    val newPath = path.fileName.toString() + "_256.png"
                    Files.move(path, path.parent.resolve(newPath))
                    ProductDao.save(product.copy(image = path.fileName.toString()))
                    it.response().end("{}")
                }, false)


        router.post("/rest/admin/alerts/testAlert")
                .blockingHandler({
                    val body = it.bodyAsString
                    val test = mapper.readValue(body, TestAlertRequest::class.java)
                    val result = AlertService.testMailAlert(test.alertType, test.cooperatorId)
                    it.response().end("{\"success\":$result}")
                }, false)

        router.post("/rest/admin/alert")
                .blockingHandler({
                    val body = it.bodyAsString
                    val request = mapper.readValue(body, SaveAlertRequest::class.java)
                    with(request) {
                        AlertService.updateAlert(alertType, message, title)
                    }
                    it.response().end("{\"success\":true}")
                }, false)

        router.post("/rest/admin/alerts/sendPasswordDefinitionAlert")
                .blockingHandler({
                    AlertService.sendPasswordDefinitionAlert()
                    it.response().end("{\"success\":true}")
                }, false)

        router.get("/rest/admin/alert/:alertType")
                .blockingHandler({
                    it.response().end(mapper.writeValueAsString(AlertService.getAlert(AlertType.valueOf(it.request().getParam("alertType")))))
                }, false)

        router.get("/rest/admin/cooperators/lessparticipation")
                .blockingHandler({
                    it.response().end(mapper.writeValueAsString(
                            AlertService.getCooperatorsWithLessParticipation().map { it.key to it.value }))
                }, false)

        router.get("/rest/products")
                .blockingHandler({
                    it.response().end(mapper.writeValueAsString(ProductDao.find().map { ProductInfo(it) }))
                }, false)

        router.get("/rest/participations/:cooperatorId").blockingHandler({
            it.response().end(mapper.writeValueAsString(CooperatorParticipationDao.findByCooperatorId(Id(it.request().getParam("cooperatorId")))))
        }, false)

        router.post("/rest/admin/participation").blockingHandler({
            val body = it.bodyAsString
            val participation = mapper.readValue(body, CooperatorParticipation::class.java)
            CooperatorParticipationDao.save(participation)
            val distribution = DistributionDao.findById(participation.distributionId)!!
            val attendance =
                    distribution
                            .attendance
                            .first { it.cooperatorId == participation.cooperatorId }
                            .copy(present = participation.present)

            DistributionDao.save(
                    distribution.copy
                    (attendance =
                    distribution.attendance.filter { it.cooperatorId != participation.cooperatorId }
                            + attendance))

            it.response().end()
        }, false)

        router.post("/rest/participation")
                .blockingHandler({
                    val body = it.bodyAsString
                    val participation = mapper.readValue(body, CooperatorParticipation::class.java)
                    CooperatorParticipationDao.save(participation)
                    val cooperator = CooperatorDao.findById(participation.cooperatorId)
                    val distribution = DistributionDao.findById(participation.distributionId)!!
                    val newAttendance = (distribution.attendance
                            .filter { it.cooperatorId != participation.cooperatorId } +
                            (if (participation.present && cooperator != null) {
                                if (participation.distributionRole != DistributionRole.Normal) {
                                    (if (cooperator.coordinator) listOf(DistributionAttendance(participation.cooperatorId, DistributionType.Vegetables, DistributionRole.Coordinator)) else emptyList()) +
                                            cooperator.contracts
                                                    .filter { it.distributionRole == DistributionRole.Referent }
                                                    .map { DistributionAttendance(participation.cooperatorId, it.contractType, DistributionRole.Referent) }
                                } else {
                                    List(participation.number) { DistributionAttendance(participation.cooperatorId, DistributionType.Vegetables, participation.distributionRole) }
                                }
                            } else emptyList()))
                    DistributionDao.save(distribution.copy(attendance = newAttendance))
                    it.response().end()
                }, false)

        router.get("/rest/distribution/:id")
                .blockingHandler({
                    val distribution = DistributionService.linkedDistributionById(Id(it.request().getParam("id")))
                    if (distribution == null) {
                        it.fail(404)
                    } else {
                        it.response().end(mapper.writeValueAsString(distribution))
                    }
                }, false)

        router.get("/rest/distributions/calendar")
                .blockingHandler({
                    val distributions = DistributionDao.find()
                    val cooperators = DistributionService.getCooperatorsInfo(distributions)
                    it.response().end(mapper.writeValueAsString(DistributionCalendar(distributions, cooperators)))
                }, false)

        router.get("/rest/currentDistribution")
                .blockingHandler({
                    val distribution = DistributionService.currentLinkedDistribution()
                    if (distribution == null) {
                        it.fail(404)
                    } else {
                        it.response().putHeader("Content-Type", "application/json")
                        it.response().end(mapper.writeValueAsString(distribution))
                    }
                }, false)

        router.get("/rest/distribution/details/:id")
                .blockingHandler({
                    val distribution = DistributionDao.findById(Id(it.request().getParam("id")))
                    if (distribution == null) {
                        it.fail(404)
                    } else {
                        val details = DistributionDetails(distribution, DistributionService.getCooperatorsInfo(distribution))
                        it.response().end(mapper.writeValueAsString(details))
                    }
                }, false)

        router.get("/rest/recipe/:id")
                .blockingHandler({
                    val recipe = RecipeDao.findById(Id(it.request().getParam("id")))
                    if (recipe == null) {
                        it.fail(404)
                    } else {
                        it.response().end(mapper.writeValueAsString(recipe))
                    }
                }, false)
        router.get("/rest/recipes/:distributionId")
                .blockingHandler({
                    val distribution = DistributionDao.findById(Id(it.request().getParam("distributionId")))
                    if (distribution == null) {
                        it.fail(404)
                    } else {
                        it.response().end(mapper.writeValueAsString(selectRecipes(distribution.products.map { it._id })))
                    }
                }, false)
        router.post("/rest/recipes")
                .blockingHandler({
                    val body = it.bodyAsString
                    val request = mapper.readValue(body, RecipesRequest::class.java)
                    if (request == null) {
                        it.fail(404)
                    } else {
                        it.response().end(mapper.writeValueAsString(selectRecipes(request.productsIds)))
                    }
                }, false)

        router.post("/rest/authenticate")
                .blockingHandler({
                    val body = it.bodyAsString
                    val request = mapper.readValue(body, AuthenticateRequest::class.java)
                    if (request == null) {
                        it.fail(404)
                    } else {
                        val session = AuthenticationService.authenticate(request.email, request.password)
                        val cooperator = if (session != null) CooperatorDao.findById(session.cooperatorId) else null
                        it.response().end(mapper.writeValueAsString(AuthenticateResponse(session?._id, cooperator)))
                    }
                }, false)

        router.post("/rest/checkSession")
                .blockingHandler({
                    val body = it.bodyAsString
                    val request = mapper.readValue(body, SessionRequest::class.java)
                    if (request == null) {
                        it.fail(404)
                    } else {
                        val cooperator = AuthenticationService.getCooperator(request.sessionId)
                        it.response().end(mapper.writeValueAsString(SessionResponse(request.sessionId, cooperator)))
                    }
                }, false)

        router.post("/rest/password-reminder")
                .blockingHandler({
                    val body = it.bodyAsString
                    val request = mapper.readValue(body, PasswordReminderRequest::class.java)
                    if (request == null) {
                        it.fail(404)
                    } else {
                        val status = AuthenticationService.passwordReminder(request.email)
                        it.response().end(mapper.writeValueAsString(PasswordReminderResponse(status)))
                    }
                }, false)

        router.get("/rest/password-reminder-token-valid/:tokenId")
                .blockingHandler({
                    val valid = AuthenticationService.isPasswordReminderTokenValid(Id(it.request().getParam("tokenId")))
                    it.response().end("{\"valid\":$valid}")
                }, false)

        router.post("/rest/change-password")
                .blockingHandler({
                    val body = it.bodyAsString
                    val request = mapper.readValue(body, ChangePasswordRequest::class.java)
                    if (request == null) {
                        it.fail(404)
                    } else {
                        val status = AuthenticationService.updatePassword(request.tokenId, request.newPassword)
                        it.response().end(mapper.writeValueAsString(ChangePasswordResponse(status)))
                    }
                }, false)

        router.post("/rest/logout")
                .blockingHandler({
                    val body = it.bodyAsString
                    try {
                        val request = mapper.readValue(body, LogoutRequest::class.java)
                        if (request == null) {
                            it.fail(404)
                        } else {
                            if (request.sessionId != null) {
                                AuthenticationService.logout(request.sessionId)
                            }
                            it.response().end("{}")
                        }
                    } catch(e: Exception) {
                        logger.error(e) { e.message }
                        it.response().end("{}")
                    }
                }, false)

        if (Env.alertCrawler) {
            AlertService.initCrawlers(vertx)
        }

        server.requestHandler { router.accept(it) }.listen(8080)
    }

    private fun selectRecipes(productIds: List<Id<Product>>): RecipesResponse {
        val recipes = RecipeDao.find()
        if (productIds.isEmpty()) {
            return RecipesResponse(productIds, recipes)
        }
        val idSet = productIds.toSet()

        val map = recipes.groupBy { it.productsIds.count { idSet.contains(it) } }.filterKeys { it != 0 }
        val result = mutableListOf<Recipe>()
        for (i in productIds.size downTo 1) {
            val value = map.get(i)
            if (value != null) {
                result.addAll(value)
            }
        }
        return RecipesResponse(productIds, result)
    }

    private fun bodyHandler(): BodyHandler {
        return BodyHandler.create()
                .setBodyLimit(10000000L)
                .setMergeFormAttributes(false)
                .setUploadsDirectory(Env.uploadPath)
    }

    private fun corsHandler(): CorsHandler {
        return CorsHandler.create("http://localhost:4200")
                .allowCredentials(true)
                .allowedHeaders(setOf("Content-Type", "Access-Control-Allow-Origin"))
                .allowedMethods(EnumSet.of(HttpMethod.GET, HttpMethod.OPTIONS, HttpMethod.POST, HttpMethod.DELETE, HttpMethod.PATCH))
    }
}