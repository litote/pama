/*
 *     PAMA
 *     Copyright (C) 2016  Litote
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as
 *     published by the Free Software Foundation, either version 3 of the
 *     License, or (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.litote.pama.util

import org.jasypt.digest.StandardStringDigester
import org.passay.CharacterRule
import org.passay.EnglishCharacterData
import org.passay.LengthRule
import org.passay.PasswordData
import org.passay.PasswordGenerator
import org.passay.PasswordValidator


/**
 *
 */
object PasswordEncryptor {

    private class StrongPasswordEncryptor {

        private val digester: StandardStringDigester

        init {
            this.digester = StandardStringDigester()
            this.digester.setAlgorithm("SHA-256")
            this.digester.setIterations(100000)
            this.digester.setSaltSizeBytes(16)
            this.digester.initialize()
        }

        fun encryptPassword(password: String): String {
            return this.digester.digest(password)
        }

        fun checkPassword(plainPassword: String, encryptedPassword: String): Boolean {
            return this.digester.matches(plainPassword, encryptedPassword)
        }

    }

    private val encryptor = StrongPasswordEncryptor()

    private val generatorRules = listOf(
            // at least one upper-case character
            CharacterRule(EnglishCharacterData.UpperCase, 1),

            // at least one lower-case character
            CharacterRule(EnglishCharacterData.LowerCase, 1),

            // at least one digit character
            CharacterRule(EnglishCharacterData.Digit, 1),

            // at least one symbol (special character)
            CharacterRule(EnglishCharacterData.Special, 1))

    private val passwordValidator = PasswordValidator(generatorRules + LengthRule(8, 16))


    fun encrypt(password: String): String {
        return encryptor.encryptPassword(password)
    }

    fun isValidPassword(password: String): Boolean {
        return passwordValidator.validate(PasswordData(password)).isValid
    }

    fun check(input: String, encrypted: String): Boolean {
        return encryptor.checkPassword(input, encrypted)
    }

}