/*
 *     PAMA
 *     Copyright (C) 2016  Litote
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as
 *     published by the Free Software Foundation, either version 3 of the
 *     License, or (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.litote.pama.util

/**
 *
 */
object Env {

    private val env = System.getenv("pama_env") ?: "production"
    val alertCrawler: Boolean =  "production" == env
    val cors: Boolean = "production" != env
    val frontUrl: String = if (env == "production") "https://amapenligne.amapversailles.fr" else "http://localhost:4200"
    val uploadPath: String = System.getProperty("pama_upload_path", "pama-web/src/assets/images")
}