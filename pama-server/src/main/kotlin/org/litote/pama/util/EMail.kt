/*
 *     PAMA
 *     Copyright (C) 2016  Litote
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as
 *     published by the Free Software Foundation, either version 3 of the
 *     License, or (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.litote.pama.util

import mu.KLogging
import java.nio.file.Files
import java.nio.file.Paths
import java.util.Properties
import javax.mail.Message
import javax.mail.MessagingException
import javax.mail.Session
import javax.mail.Transport
import javax.mail.internet.InternetAddress
import javax.mail.internet.MimeMessage


/**
 *
 */
object EMail : KLogging() {

    private val mailProperties = Properties()

    init {
        val mailConfiguration = System.getenv("pama_mail_configuration") ?: "./conf/mail.properties"
        logger.info("Mail configuration file : $mailConfiguration")
        try {
            val inputStream = Files.newInputStream(Paths.get(mailConfiguration))
            mailProperties.load(inputStream)
        } catch(e: Exception) {
            logger.error("Unable to load mail properties file / please provide the path with env var \"pama_mail_configuration\"", e)
        }
    }

    fun send(subject: String, text: String, to: InternetAddress): Boolean {
        return send(subject, text, listOf(to), emptyList())
    }

    fun send(subject: String, text: String, to: List<InternetAddress>, cc: List<InternetAddress> = emptyList()): Boolean {
        if (to.isEmpty()) {
            logger.warn { "Empty 'to' list for mail $subject" }
            return false
        }
        try {
            val session = Session.getInstance(mailProperties)
            session.debug = mailProperties.getProperty("mail.smtp.debug", "false") == "true"

            val message = MimeMessage(session)
            try {
                message.setFrom(InternetAddress(mailProperties.getProperty("mail.from"), "AMAP Versailles"))
                if (cc.isNotEmpty()) {
                    message.setRecipients(Message.RecipientType.CC, cc.toTypedArray())
                }
                message.setText(text)
                message.subject = subject
                message.addRecipients(Message.RecipientType.TO, to.toTypedArray())
            } catch (e: MessagingException) {
                logger.error(e, { e.message })
                return false
            }

            logger.info { "send message to ${message.getAllRecipients().joinToString()}..." }
            Transport.send(message, mailProperties.getProperty("mail.user"), mailProperties.getProperty("mail.password"))
            logger.info { "message sent!" }
            return true
        } catch (t: Throwable) {
            logger.error(t, { t.message })
            return false
        }
    }
}