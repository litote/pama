/*
 *     PAMA
 *     Copyright (C) 2016  Litote
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as
 *     published by the Free Software Foundation, either version 3 of the
 *     License, or (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.litote.pama.data.json.alert

import org.litote.pama.data.json.JsonCollectionDao
import org.litote.pama.data.json.JsonDao
import org.litote.pama.data.model.alert.AlertDefinition
import org.litote.pama.data.model.alert.AlertType

/**
 *
 */
object AlertDefinitionDao : JsonCollectionDao<AlertDefinition> {

    override val name: String = "alertDefinition"

    fun find(): List<AlertDefinition> = JsonDao.load(this)

    fun findByType(alertType: AlertType): AlertDefinition? = find().firstOrNull { it.alertType == alertType }

    fun save(alertDefinition: AlertDefinition) {
        save((find().filter { it._id != alertDefinition._id } + alertDefinition))
    }

    fun save(list: List<AlertDefinition>) = JsonDao.save(this, list)
}