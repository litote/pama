/*
 *     PAMA
 *     Copyright (C) 2016  Litote
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as
 *     published by the Free Software Foundation, either version 3 of the
 *     License, or (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.litote.pama.data.json

import org.litote.pama.data.model.Distribution
import org.litote.pama.data.model.Id
import org.litote.pama.data.model.isEmpty

/**
 *
 */
object DistributionDao : JsonCollectionDao<Distribution> {

    override val name: String = "distribution"

    fun find(): List<Distribution> = JsonDao.load(this)

    fun findById(id: Id<Distribution>): Distribution? = find().firstOrNull { it._id == id }

    fun deleteById(id: Id<Distribution>) {
        save(find().filter { it._id != id })
    }

    fun save(distribution: Distribution) {
        val dist = if (distribution._id.isEmpty()) distribution.copy(_id = Id()) else distribution
        save((find().filter { it._id != dist._id } + dist.copy(products = dist.products.sortedWith(ProductDao.nameComparator))))
    }

    fun save(list: List<Distribution>) = JsonDao.save(this, list.sortedBy { it.date })
}