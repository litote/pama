/*
 *     PAMA
 *     Copyright (C) 2016  Litote
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as
 *     published by the Free Software Foundation, either version 3 of the
 *     License, or (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.litote.pama.data.model.alert

import org.litote.pama.data.model.Id
import java.time.DayOfWeek

/**
 *
 */
data class AlertDefinition(val _id: Id<AlertDefinition> = Id(),
                           val alertType: AlertType,
                           val day: DayOfWeek,
                           val hourOfDay: Int,
                           val message: String,
                           val title: String,
                           val disabled: Boolean = false,
                           val crawl: Boolean = true) {
}