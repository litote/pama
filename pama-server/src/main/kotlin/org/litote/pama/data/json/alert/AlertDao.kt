/*
 *     PAMA
 *     Copyright (C) 2016  Litote
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as
 *     published by the Free Software Foundation, either version 3 of the
 *     License, or (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.litote.pama.data.json.alert

import org.litote.pama.data.json.JsonCollectionDao
import org.litote.pama.data.json.JsonDao
import org.litote.pama.data.model.alert.Alert
import org.litote.pama.data.model.alert.AlertType

/**
 *
 */
object AlertDao : JsonCollectionDao<Alert> {

    override val name: String = "alert"

    fun find(): List<Alert> = JsonDao.load(this)

    fun findByAlertType(alertType: AlertType): List<Alert> = find().filter { it.alertType == alertType }

    fun findLast(alertType: AlertType): Alert? = findByAlertType(alertType).sortedBy { it.date }.lastOrNull()

    fun save(alert: Alert) {
        save((find().filter { it._id != alert._id } + alert))
    }

    fun save(list: List<Alert>) = JsonDao.save(this, list)
}