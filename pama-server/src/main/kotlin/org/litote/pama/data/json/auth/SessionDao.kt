/*
 *     PAMA
 *     Copyright (C) 2016  Litote
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as
 *     published by the Free Software Foundation, either version 3 of the
 *     License, or (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.litote.pama.data.json.auth

import org.litote.pama.data.json.JsonCollectionDao
import org.litote.pama.data.json.JsonDao
import org.litote.pama.data.model.Id
import org.litote.pama.data.model.auth.Session
import java.time.LocalDateTime

/**
 *
 */
object SessionDao : JsonCollectionDao<Session> {

    override val name: String = "session"

    fun find(): List<Session> = JsonDao.load(this)

    fun findById(id: Id<Session>): Session? = find().firstOrNull { it._id == id }

    fun removeById(id: Id<Session>) {
        save(find().filter { it._id != id })
    }

    fun save(session: Session) {
        save(find().filter { it._id != session._id } + session)
    }

    fun save(list: List<Session>) {
        val now = LocalDateTime.now();
        JsonDao.save(this, list.filter { it.expiration > now })
    }
}