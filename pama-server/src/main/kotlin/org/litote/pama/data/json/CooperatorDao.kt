/*
 *     PAMA
 *     Copyright (C) 2016  Litote
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as
 *     published by the Free Software Foundation, either version 3 of the
 *     License, or (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.litote.pama.data.json

import org.litote.pama.data.model.Contract
import org.litote.pama.data.model.Cooperator
import org.litote.pama.data.model.Id
import java.text.Collator
import java.util.Locale
import kotlin.comparisons.compareBy

/**
 *
 */
object CooperatorDao : JsonCollectionDao<Cooperator> {

    override val name: String = "cooperator"

    fun find(): List<Cooperator> = JsonDao.load(this)

    fun findById(id: Id<Cooperator>): Cooperator? = find().firstOrNull { it._id == id }

    fun deleteById(id: Id<Cooperator>) = save(find().filter { it._id != id })

    fun findByEmail(email: String): Cooperator? = find().firstOrNull { it.email.equals(email.trim().toLowerCase()) }

    fun save(cooperator: Cooperator) {
        save((find().filter { it._id != cooperator._id } + cooperator.copy(email = cooperator.email?.trim()?.toLowerCase())))
    }

    fun save(list: List<Cooperator>) = JsonDao.save(this, list.sortedWith(compareBy(Collator.getInstance(Locale.FRENCH), { it.lastName })))

    fun findContracts(): List<Contract> = find().flatMap { it.contracts }.distinctBy { it._id }
}