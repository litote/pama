/*
 *     PAMA
 *     Copyright (C) 2016  Litote
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as
 *     published by the Free Software Foundation, either version 3 of the
 *     License, or (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.litote.pama.data.rest

import org.litote.pama.data.model.DistributionType
import org.litote.pama.data.model.Id
import org.litote.pama.data.model.Product

/**
 *
 */
data class ProductInfo(val name: String,
                  val image: String?,
                  val _id: Id<Product>?,
                  val cuisineLibreKeyword: String?,
                  val type: DistributionType) {

    constructor(product: Product) : this(product.name, product.image, product._id, product.cuisineLibreKeyword, product.type)

    fun toProduct(): Product {
        return Product(name, image, _id ?: Id(), cuisineLibreKeyword, type)
    }
}