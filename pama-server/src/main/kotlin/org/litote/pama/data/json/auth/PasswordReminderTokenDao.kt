/*
 *     PAMA
 *     Copyright (C) 2016  Litote
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as
 *     published by the Free Software Foundation, either version 3 of the
 *     License, or (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.litote.pama.data.json.auth

import org.litote.pama.data.json.JsonCollectionDao
import org.litote.pama.data.json.JsonDao
import org.litote.pama.data.model.Cooperator
import org.litote.pama.data.model.Id
import org.litote.pama.data.model.auth.PasswordReminderToken
import java.time.LocalDateTime.now

/**
 *
 */
object PasswordReminderTokenDao : JsonCollectionDao<PasswordReminderToken> {

    override val name: String = "passwordReminderToken"

    fun find(): List<PasswordReminderToken> = JsonDao.load(this)

    fun findById(id: Id<PasswordReminderToken>): PasswordReminderToken? = find().firstOrNull { it._id == id }

    fun findByCooperatorId(id: Id<Cooperator>): PasswordReminderToken? = find().firstOrNull { it.cooperatorId == id }

    fun remove(password: PasswordReminderToken) {
        save(find().filter { it.cooperatorId != password.cooperatorId })
    }

    fun save(password: PasswordReminderToken) {
        save(find().filter { it.cooperatorId != password.cooperatorId } + password)
    }

    fun save(list: List<PasswordReminderToken>) {
        val now = now();
        JsonDao.save(this, list.filter { it.expiration > now })
    }
}