/*
 *     PAMA
 *     Copyright (C) 2016  Litote
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as
 *     published by the Free Software Foundation, either version 3 of the
 *     License, or (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.litote.pama.data.json

import org.litote.pama.data.model.Cooperator
import org.litote.pama.data.model.CooperatorParticipation
import org.litote.pama.data.model.Id

/**
 *
 */
object CooperatorParticipationDao : JsonCollectionDao<CooperatorParticipation> {

    override val name: String = "participation"

    fun find(): List<CooperatorParticipation> = JsonDao.load(this)

    fun findByCooperatorId(id: Id<Cooperator>): List<CooperatorParticipation> = find().filter { it.cooperatorId == id }

    fun save(participation: CooperatorParticipation) {
        save(find().filter { it.cooperatorId != participation.cooperatorId || it.distributionId != participation.distributionId } + participation)
    }

    fun save(list: List<CooperatorParticipation>) = JsonDao.save(this, list)
}