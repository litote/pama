/*
 *     PAMA
 *     Copyright (C) 2016  Litote
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as
 *     published by the Free Software Foundation, either version 3 of the
 *     License, or (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.litote.pama.data.json.auth

import org.litote.pama.data.json.JsonCollectionDao
import org.litote.pama.data.json.JsonDao
import org.litote.pama.data.model.Cooperator
import org.litote.pama.data.model.Id
import org.litote.pama.data.model.auth.PasswordStorage

/**
 *
 */
object PasswordStorageDao : JsonCollectionDao<PasswordStorage> {

    override val name: String = "passwordStorage"

    fun find(): List<PasswordStorage> = JsonDao.load(this)

    fun findByCooperatorId(id: Id<Cooperator>): PasswordStorage? = find().firstOrNull { it.cooperatorId == id }

    fun save(password: PasswordStorage) {
        save(find().filter { it.cooperatorId != password.cooperatorId } + password)
    }

    fun save(list: List<PasswordStorage>) = JsonDao.save(this, list)
}