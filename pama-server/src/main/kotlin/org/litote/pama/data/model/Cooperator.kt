/*
 *     PAMA
 *     Copyright (C) 2016  Litote
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as
 *     published by the Free Software Foundation, either version 3 of the
 *     License, or (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.litote.pama.data.model

import com.fasterxml.jackson.annotation.JsonIgnore
import javax.mail.internet.InternetAddress

/**
 *
 */
data class Cooperator(val firstName: String,
                      val lastName: String,
                      val email: String?,
                      val telephone: String? = null,
                      val address: String? = null,
                      val role: CooperatorRole = CooperatorRole.Cooperator,
                      val contracts: List<Contract> = emptyList(),
                      val _id: Id<Cooperator> = Id(),
                      val coordinator: Boolean = false,
                      val additionalCooperators: Int = 0) {

    @JsonIgnore
    fun isReferent(): Boolean = contracts.any { it.distributionRole == DistributionRole.Referent }

    fun completeName(): String = "$firstName $lastName"

    fun internetAddress(): InternetAddress = InternetAddress(email, completeName())
}