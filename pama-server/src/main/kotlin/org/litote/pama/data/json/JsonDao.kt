/*
 *     PAMA
 *     Copyright (C) 2016  Litote
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as
 *     published by the Free Software Foundation, either version 3 of the
 *     License, or (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.litote.pama.data.json

import mu.KLogging
import org.litote.pama.jackson.JacksonMapperProvider.mapper
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.util.concurrent.ConcurrentHashMap

/**
 *
 */
object JsonDao : KLogging() {

    private val rootPath: Path = Paths.get(System.getProperty("amap.db.rootpath", "./jsondb"))
    val cache = ConcurrentHashMap<JsonCollectionDao<*>, List<*>>()

    init {
        Files.createDirectories(rootPath)
        logger.info { "db root path : ${rootPath.toAbsolutePath()}" }
    }

    fun <T> collectionPath(collection: JsonCollectionDao<T>): Path {
        return rootPath.resolve(collection.name + ".json")
    }

    inline fun <reified T : Any> load(collection: JsonCollectionDao<T>): List<T> {
        @Suppress("UNCHECKED_CAST")
        return cache.getOrPut(collection, {
            val colPath = collectionPath(collection)
            if(!Files.exists(colPath)) {
                return emptyList()
            }
            val content = Files.readAllBytes(colPath)
            return if (content == null) {
                emptyList()
            } else {
                mapper.readValue(content, mapper.typeFactory.constructCollectionType(List::class.java, T::class.java));
            }
        }) as List<T>
    }

    fun <T> save(collection: JsonCollectionDao<T>, list: List<T>) {
        val colPath = collectionPath(collection)
        cache.put(collection, list)
        Files.write(colPath, mapper.writeValueAsBytes(list))
    }
}