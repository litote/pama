/*
 *     PAMA
 *     Copyright (C) 2016  Litote
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as
 *     published by the Free Software Foundation, either version 3 of the
 *     License, or (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.litote.pama.data.model

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.SerializerProvider
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import com.fasterxml.jackson.databind.annotation.JsonSerialize
import com.fasterxml.jackson.databind.deser.std.StdDeserializer
import com.fasterxml.jackson.databind.ser.std.StdSerializer
import java.util.UUID

private class IdDeserializer : StdDeserializer<Id<*>>(Id::class.java) {

    override fun deserialize(jp: JsonParser, ctxt: DeserializationContext): Id<*>? {
        val id = jp.getCodec().readValue(jp, String::class.java)
        if (id == null) return null else return Id<Any>(id)
    }
}

private class IdSerializer : StdSerializer<Id<*>>(Id::class.java) {

    override fun serialize(value: Id<*>?, gen: JsonGenerator, provider: SerializerProvider) {
        if (value == null) {
            gen.writeNull()
        } else {
            gen.writeString(value.value)
        }
    }
}

/**
 *
 */
@JsonDeserialize(using = IdDeserializer::class)
@JsonSerialize(using = IdSerializer::class)
data class Id<R>(val value: String) {

    constructor() : this(UUID.randomUUID().toString())

}

fun Id<*>?.isEmpty(): Boolean = this == null || value.isNullOrBlank()