/*
 *     PAMA
 *     Copyright (C) 2016  Litote
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as
 *     published by the Free Software Foundation, either version 3 of the
 *     License, or (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.litote.pama.data.json

import org.litote.pama.data.model.Id
import org.litote.pama.data.model.Product
import org.litote.pama.data.model.isEmpty
import java.text.Collator
import java.util.Locale
import kotlin.comparisons.compareBy

/**
 *
 */
object ProductDao : JsonCollectionDao<Product> {

    val nameComparator = compareBy<Product, String>(Collator.getInstance(Locale.FRANCE), { it.name })

    override val name: String = "product"

    fun find(): List<Product> = JsonDao.load(this)

    fun findById(id: Id<Product>): Product? = find().firstOrNull { it._id == id }

    fun save(product: Product) {
        val p = if (product._id.isEmpty()) product.copy(_id = Id()) else product
        save((find().filter { it._id != p._id } + p))
    }

    fun save(list: List<Product>) = JsonDao.save(this, list.sortedWith(nameComparator))
}