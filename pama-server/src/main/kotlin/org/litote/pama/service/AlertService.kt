/*
 *     PAMA
 *     Copyright (C) 2016  Litote
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as
 *     published by the Free Software Foundation, either version 3 of the
 *     License, or (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.litote.pama.service

import io.vertx.core.Vertx
import mu.KLogging
import org.litote.pama.data.json.CooperatorDao
import org.litote.pama.data.json.CooperatorParticipationDao
import org.litote.pama.data.json.alert.AlertDao
import org.litote.pama.data.json.alert.AlertDefinitionDao
import org.litote.pama.data.json.auth.PasswordStorageDao
import org.litote.pama.data.model.Cooperator
import org.litote.pama.data.model.Distribution
import org.litote.pama.data.model.DistributionRole
import org.litote.pama.data.model.Id
import org.litote.pama.data.model.alert.Alert
import org.litote.pama.data.model.alert.AlertDefinition
import org.litote.pama.data.model.alert.AlertType
import org.litote.pama.util.EMail
import java.time.Duration
import java.time.LocalDateTime
import java.time.OffsetDateTime
import java.time.format.DateTimeFormatter
import java.time.temporal.TemporalAdjusters
import java.util.SortedMap
import java.util.TreeMap
import java.util.concurrent.ConcurrentHashMap

/**
 *
 */
object AlertService : KLogging() {

    private class AlertCheckerTask(val alertType: AlertType) {

        fun check() {
            try {
                logger.debug { "check alert : $alertType" }
                val nextDistribution = DistributionService.currentDistribution()
                if (nextDistribution != null) {
                    val lastAlert = AlertDao.findLast(alertType)
                    if (lastAlert == null || lastAlert.distributionId != nextDistribution._id) {
                        val alertDefinition = AlertDefinitionDao.findByType(alertType)!!

                        val sendResult = when (alertType) {
                            AlertType.NextDistribution -> sendNextDistributionAlert(alertDefinition, nextDistribution)
                            AlertType.CallForVolunteers -> sendCallForVolunteersAlert(alertDefinition, nextDistribution)
                            AlertType.DefinePassword -> false
                        }
                        if (sendResult) {
                            AlertDao.save(Alert(alertDefinition._id, alertType, OffsetDateTime.now(), nextDistribution._id))
                        }
                    }
                }
            } catch(t: Throwable) {
                logger.error(t.message, t)
            }
        }
    }

    private val subjectDateFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy")
    private val messageDateFormatter = DateTimeFormatter.ofPattern("EEEE dd MMMM yyyy")

    private val alertCheckerMap = ConcurrentHashMap<AlertType, AlertCheckerTask>()


    fun initCrawlers(vertx: Vertx) {
        vertx.executeBlocking<Unit>({
            AlertType.values().forEach {
                val alertDefinition = AlertDefinitionDao.findByType(it)
                if (alertDefinition != null && alertDefinition.crawl && !alertDefinition.disabled) {
                    try {
                        logger.info { "Init checker for alert $it" }
                        val task = AlertCheckerTask(it)
                        alertCheckerMap.put(it, task)
                        val fromNowNextCall = Duration.between(
                                LocalDateTime.now(),
                                LocalDateTime.now().with(TemporalAdjusters.next(alertDefinition.day)).withHour(alertDefinition.hourOfDay))
                        vertx.setTimer(fromNowNextCall.toMillis(), {
                            task.check()
                            vertx.setPeriodic(Duration.ofDays(7).toMillis()) {
                                task.check()
                            }
                        })
                    } catch(t: Throwable) {
                        logger.error(t.message, t)
                    }
                }
            }

            it.complete()
        }, {})

    }


    fun testMailAlert(alertType: AlertType, cooperatorId: Id<Cooperator>): Boolean {
        val alertDefinition = AlertDefinitionDao.findByType(alertType)!!
        val cooperator = CooperatorDao.findById(cooperatorId)!!
        return when (alertType) {
            AlertType.NextDistribution, AlertType.CallForVolunteers -> {
                sendDistributionAlert(
                        alertDefinition,
                        DistributionService.currentDistribution()!!,
                        setOf(cooperator))
            }
            AlertType.DefinePassword -> sendPasswordDefinitionAlert(cooperator, alertDefinition)
        }
    }


    fun sendNextDistributionAlert(alertDefinition: AlertDefinition, distribution: Distribution): Boolean {
        return sendDistributionAlert(
                alertDefinition,
                distribution,
                getCooperatorsForDistribution(distribution))

    }

    fun sendCallForVolunteersAlert(alertDefinition: AlertDefinition, distribution: Distribution): Boolean {
        return if (getCooperatorsForDistribution(distribution).size >= 8) {
            logger.info { "More than 8 cooperators for next distribution - skip call for volunteers alert" }
            true
        } else {
            sendDistributionAlert(
                    alertDefinition,
                    distribution,
                    getCooperatorsWithLessParticipation().values.flatMap { it }.toSet())
        }
    }

    private fun sendPasswordDefinitionAlert(cooperator: Cooperator, alertDefinition: AlertDefinition): Boolean {
        return AuthenticationService.requestPasswordCreation(cooperator) {
            alertDefinition.title to alertDefinition.message.replace("{password_change}", it)
        }
    }

    fun sendPasswordDefinitionAlert() {
        val alertDefinition = AlertDefinitionDao.findByType(AlertType.DefinePassword)
        if (alertDefinition != null) {
            CooperatorDao.find()
                    .filter { PasswordStorageDao.findByCooperatorId(it._id) == null }
                    .forEach {
                        sendPasswordDefinitionAlert(it, alertDefinition)
                    }
        } else {
            logger.warn { "no alert DefinePassword" }
        }
    }

    private fun getCooperatorsForDistribution(distribution: Distribution): Set<Cooperator> {
        return distribution.attendance
                .filter { it.role == DistributionRole.Normal }
                .mapNotNull { CooperatorDao.findById(it.cooperatorId) }
                .toSet()
    }

    fun getCooperatorsWithLessParticipation(): SortedMap<Int, List<Cooperator>> {
        val cooperatorsMap = CooperatorDao.find().map { it._id to it }.toMap()
        val participationsGroupByCooperators =
                CooperatorParticipationDao
                        .find()
                        .filter { it.present }
                        .groupBy { it.cooperatorId }


        val result = TreeMap<Int, List<Cooperator>>()
        val noParticipation =
                cooperatorsMap
                        .filterNot { participationsGroupByCooperators.containsKey(it.key) }
        if (noParticipation.isNotEmpty()) {
            result.put(0, noParticipation.values.sortedBy { it.lastName })
        }
        if (noParticipation.size < 20) {
            var count = 0
            val iterator = participationsGroupByCooperators
                    .filter { cooperatorsMap.containsKey(it.key) }
                    .map { it.value.size to cooperatorsMap.get(it.key)!! }
                    .groupBy({ it.first }, { it.second })
                    .toSortedMap().iterator()
            while (count < 20 && iterator.hasNext()) {
                iterator.next().let {
                    count += it.value.size
                    result.put(it.key, it.value.sortedBy { it.lastName })
                }
            }
        }

        return result
    }

    private fun sendDistributionAlert(alertDefinition: AlertDefinition, distribution: Distribution, cooperators: Set<Cooperator>): Boolean {
        logger.info { "send alert for ${alertDefinition.alertType} & ${distribution._id}" }
        val message = alertDefinition.message.replace("{date_distribution}", distribution.date.format(messageDateFormatter))
        val subject = alertDefinition.title.replace("{date_distribution}", distribution.date.format(subjectDateFormatter))
        return EMail.send(
                subject,
                message,
                cooperators.map { it.internetAddress() })
    }

    fun getAlert(alertType: AlertType): AlertDefinition? {
        return AlertDefinitionDao.findByType(alertType)
    }

    fun updateAlert(alertType: AlertType, message: String, title: String) {
        AlertDefinitionDao.save(AlertDefinitionDao.findByType(alertType)!!.copy(message = message, title = title))
    }

}