/*
 *     PAMA
 *     Copyright (C) 2016  Litote
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as
 *     published by the Free Software Foundation, either version 3 of the
 *     License, or (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.litote.pama.service

import org.litote.pama.data.json.CooperatorDao
import org.litote.pama.data.model.Cooperator
import org.litote.pama.data.rest.cooperator.SaveCooperatorRequest
import org.litote.pama.data.rest.cooperator.SaveCooperatorResponse

/**
 *
 */
object CooperatorService {

    fun saveCooperatorByAdmin(request: SaveCooperatorRequest): SaveCooperatorResponse {
        val sameEmail = CooperatorDao.findByEmail(request.cooperator.email!!)
        if(sameEmail != null && sameEmail._id != request.cooperator._id) {
            return SaveCooperatorResponse(false, "Un amapien avec cet email existe déjà")
        }
        CooperatorDao.save(request.cooperator)
        return SaveCooperatorResponse()
    }

    fun saveCooperator(request: SaveCooperatorRequest, oldCooperator: Cooperator): SaveCooperatorResponse {
        val sameEmail = CooperatorDao.findByEmail(request.cooperator.email!!)
        if(sameEmail != null && sameEmail._id != request.cooperator._id) {
            return SaveCooperatorResponse(false, "Un amapien avec cet email existe déjà")
        }

        with(request.cooperator) {
            CooperatorDao.save(
                    oldCooperator.copy(
                            firstName = firstName,
                            lastName = lastName,
                            email = email,
                            telephone = telephone,
                            address = address,
                            additionalCooperators = additionalCooperators))
        }
        return SaveCooperatorResponse()
    }
}