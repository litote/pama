/*
 *     PAMA
 *     Copyright (C) 2016  Litote
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as
 *     published by the Free Software Foundation, either version 3 of the
 *     License, or (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.litote.pama.service

import org.litote.pama.data.json.CooperatorDao
import org.litote.pama.data.json.DistributionDao
import org.litote.pama.data.model.Distribution
import org.litote.pama.data.model.Id
import org.litote.pama.data.rest.CooperatorInfo
import org.litote.pama.data.rest.LinkedDistribution
import java.time.LocalDateTime
import java.time.temporal.ChronoUnit

/**
 *
 */
object DistributionService {

    fun currentDistribution(): Distribution? {
        val now = LocalDateTime.now().plusHours(2)
        val all = DistributionDao.find()
        return all.filter { it.date > now }.minBy { ChronoUnit.HOURS.between(now, it.date) }
    }

    fun currentLinkedDistribution(): LinkedDistribution? {
        val distribution = currentDistribution()
        return if (distribution == null) null else linkedDistribution(DistributionDao.find(), distribution)
    }

    fun linkedDistributionById(distributionId: Id<Distribution>): LinkedDistribution? {
        val distribution = DistributionDao.findById(distributionId)
        return if (distribution == null) null else linkedDistribution(DistributionDao.find(), distribution)
    }

    private fun linkedDistribution(all: List<Distribution>, current: Distribution): LinkedDistribution {
        val allExceptCurrent = all.filter { it != current }
        val previous = allExceptCurrent.filter { it.date < current.date }.minBy { ChronoUnit.HOURS.between(it.date, current.date) }
        val next = allExceptCurrent.filter { it.date > current.date }.minBy { ChronoUnit.HOURS.between(current.date, it.date) }
        return LinkedDistribution(current, getCooperatorsInfo(current), previous?._id, next?._id)
    }

    fun getCooperatorsInfo(distribution: Distribution): List<CooperatorInfo> = getCooperatorsInfo(listOf(distribution))

    fun getCooperatorsInfo(distributions: List<Distribution>): List<CooperatorInfo> {
        return distributions
                .flatMap { it.attendance }
                .map { it.cooperatorId }
                .distinct()
                .mapNotNull { CooperatorDao.findById(it) }
                .map { CooperatorInfo(it._id, it.firstName, it.lastName, it.email, it.telephone) }
    }
}