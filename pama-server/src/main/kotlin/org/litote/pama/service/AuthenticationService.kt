/*
 *     PAMA
 *     Copyright (C) 2016  Litote
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as
 *     published by the Free Software Foundation, either version 3 of the
 *     License, or (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.litote.pama.service

import org.litote.pama.data.json.CooperatorDao
import org.litote.pama.data.json.auth.PasswordReminderTokenDao
import org.litote.pama.data.json.auth.PasswordStorageDao
import org.litote.pama.data.json.auth.SessionDao
import org.litote.pama.data.model.Cooperator
import org.litote.pama.data.model.Id
import org.litote.pama.data.model.auth.PasswordReminderToken
import org.litote.pama.data.model.auth.PasswordStorage
import org.litote.pama.data.model.auth.Session
import org.litote.pama.service.AuthenticationService.PasswordReminderStatus.EmailNotFound
import org.litote.pama.service.AuthenticationService.PasswordReminderStatus.Error
import org.litote.pama.service.AuthenticationService.PasswordReminderStatus.MailSent
import org.litote.pama.service.AuthenticationService.PasswordReminderStatus.TokenExists
import org.litote.pama.util.EMail.send
import org.litote.pama.util.Env
import org.litote.pama.util.PasswordEncryptor
import java.time.LocalDateTime.now

/**
 *
 */
object AuthenticationService {

    enum class PasswordReminderStatus {
        EmailNotFound,
        TokenExists,
        MailSent,
        Error
    }

    enum class ChangePasswordStatus {
        ExpiredToken,
        InvalidPassword,
        PasswordChanged
    }

    fun requestPasswordCreation(cooperator: Cooperator, messageGenerator: (String) -> Pair<String, String>): Boolean {
        val existingToken = PasswordReminderTokenDao.findByCooperatorId(cooperator._id)
        if (existingToken != null) {
            PasswordReminderTokenDao.remove(existingToken)
        }
        val token = PasswordReminderToken(cooperator._id, now().plusDays(3))
        val url = "${Env.frontUrl}/modifier-mot-de-passe/${token._id.value}"
        val (title, message) = messageGenerator.invoke(url)
        if (send(title, message,
                cooperator.internetAddress())) {
            PasswordReminderTokenDao.save(token)
            return true
        } else {
            return false
        }
    }

    fun requestPasswordCreation(cooperator: Cooperator): Boolean {
        return requestPasswordCreation(cooperator) { url ->
            "[AMAP Versailles] Paramétrage de votre mot de passe" to
                    """
Bonjour,

Vous êtes inscrit à l'application AMAP Versailles.
Pour choisir votre mot de passe, veuillez cliquer sur ce lien : $url

AMAP Versailles"""
        }
    }

    fun passwordReminder(email: String): PasswordReminderStatus {
        val cooperator = CooperatorDao.findByEmail(email)
        if (cooperator != null) {
            val existingToken = PasswordReminderTokenDao.findByCooperatorId(cooperator._id)
            if (existingToken != null && existingToken.expiration > now()) {
                return TokenExists
            } else {
                val token = PasswordReminderToken(cooperator._id, now().plusDays(1))
                val url = "${Env.frontUrl}/modifier-mot-de-passe/${token._id.value}"
                if (send("Réinitialisation de votre mot de passe",
                        """
Bonjour,

Vous avez demandé la réinitialisation de votre mot de passe pour l'application AMAP Versailles.
Pour changer votre mot de passe, veuillez cliquer sur ce lien : $url

AMAP Versailles""",
                        cooperator.internetAddress())) {
                    PasswordReminderTokenDao.save(token)
                    return MailSent
                } else {
                    return Error
                }
            }
        } else {
            return EmailNotFound
        }
    }

    fun isPasswordReminderTokenValid(tokenId: Id<PasswordReminderToken>): Boolean {
        val now = now()
        return PasswordReminderTokenDao.findById(tokenId)?.expiration ?: now > now
    }

    fun updatePassword(tokenId: Id<PasswordReminderToken>, newPassword: String): ChangePasswordStatus {
        if (!PasswordEncryptor.isValidPassword(newPassword)) {
            return ChangePasswordStatus.InvalidPassword
        }

        val now = now()
        val token = PasswordReminderTokenDao.findById(tokenId)
        if (token?.expiration ?: now > now) {
            PasswordReminderTokenDao.remove(token!!)
            val storage = PasswordStorageDao.findByCooperatorId(token.cooperatorId)
            val encryptedPassword = PasswordEncryptor.encrypt(newPassword)
            if (storage == null) {
                PasswordStorageDao.save(PasswordStorage(token.cooperatorId, encryptedPassword))
            } else {
                PasswordStorageDao.save(storage.copy(password = encryptedPassword))
            }

            val cooperator = CooperatorDao.findById(token.cooperatorId)
            send("Modification de votre mot de passe",
                    """
Bonjour,

Votre mot de passe pour l'application AMAP Versailles a bien été modifié.

AMAP Versailles""", cooperator!!.internetAddress())

            return ChangePasswordStatus.PasswordChanged
        } else {
            return ChangePasswordStatus.ExpiredToken
        }
    }

    fun authenticate(email: String, password: String): Session? {
        if (PasswordEncryptor.isValidPassword(password)) {
            val cooperator = CooperatorDao.findByEmail(email)
            if (cooperator != null) {
                val passwordStorage = PasswordStorageDao.findByCooperatorId(cooperator._id)
                if (passwordStorage != null && PasswordEncryptor.check(password, passwordStorage.password)) {
                    val session = Session(cooperator._id, now().plusMonths(1))
                    SessionDao.save(session)
                    return session
                }
            }
        }

        return null
    }

    fun logout(sessionId: Id<Session>) {
        SessionDao.removeById(sessionId)
    }

    fun getCooperator(sessionId: Id<Session>): Cooperator? {
        val session = SessionDao.findById(sessionId)
        if (session != null && session.expiration > now()) {
            return CooperatorDao.findById(session.cooperatorId)
        }

        return null
    }

}