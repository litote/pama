# Projet PaMa - Pour aider l'aMaP

L'objectif de départ de ce projet est de fournir une application web/mobile
permettant de soulager les bénévoles des AMAPs des tâches administratives.

Un deuxième objectif est de fournir aux adhérents une interface intuitive
 pour accéder aux informations utiles concernant l'AMAP.
  
Le projet est en cours d'initialisation et ne concerne pour l'instant que l'AMAP de Versailles.

La licence sous laquelle est publiée le code source est la licence [AGPL](https://fr.wikipedia.org/wiki/GNU_Affero_General_Public_License) 