import {Component, OnInit} from "@angular/core";
import {LinkedDistribution} from "./model/distribution";
import {ActivatedRoute, Params} from "@angular/router";
import {DistributionService} from "./core/distribution.service";
import {Product} from "./model/product";
import {LoggedCooperator} from "./core/logged-cooperator";

@Component({
  selector: 'home',
  templateUrl: './home.component.html',
  styleUrls: ['./commons.css', './home.component.css']
})
export class HomeComponent implements OnInit {

  distribution: LinkedDistribution;
  selectedProduct: Product;
  gridWidth: number;

  constructor(public loggedCooperator: LoggedCooperator,
              private route: ActivatedRoute,
              private distributionService: DistributionService) {
  }

  ngOnInit() {
    this.route.queryParams.forEach((params: Params) => {
      const id = params['distributionId'];
      if (id) {
        this.distributionService.getDistribution(id).subscribe(
          distribution => this.load(distribution),
          error => window.alert(error)
        );
      } else {
        this.distributionService.getCurrentDistribution().subscribe(
          distribution => this.load(distribution),
          error => window.alert(error)
        );
      }
    });
  }

  private load(distribution: LinkedDistribution) {
    if (distribution.distribution.hasProduct()) {
      this.selectedProduct = distribution.distribution.products[0];
    } else {
      this.selectedProduct = null;
    }

    this.onResize(window.innerWidth);
    this.distribution = distribution;
  }

  onResize(innerWidth: number) {
    this.gridWidth = innerWidth - (innerWidth % 128);
  }

  selectProduct(value: Product) {
    this.selectedProduct = value;
  }

}
