import {Cooperator} from "./cooperator";
export class AuthenticateRequest {
  public constructor(public email: string, public password:string) {
  }
}

export class AuthenticateResponse {
  public constructor(public sessionId?: string, public cooperator?:Cooperator) {
  }

  static fromJSON(json?: any): AuthenticateResponse {
    if(!json) {
      return null;
    }
    let response = Object.create(AuthenticateResponse.prototype);
    // copy all the fields from the json object
    //noinspection TypeScriptValidateTypes
    return Object.assign(response, json, {
      // convert fields that need converting
      cooperator: Cooperator.fromJSON(json.cooperator)
    });
  }
}

export class SessionRequest {
  public constructor(public sessionId: string) {
  }
}

export class SessionResponse {
  public constructor(public sessionId: string, public cooperator?:Cooperator) {
  }

  static fromJSON(json?: any): SessionResponse {
    if(!json) {
      return null;
    }
    let response = Object.create(SessionResponse.prototype);
    // copy all the fields from the json object
    //noinspection TypeScriptValidateTypes
    return Object.assign(response, json, {
      // convert fields that need converting
      cooperator: Cooperator.fromJSON(json.cooperator)
    });
  }
}

export class LogoutRequest {
  public constructor(public sessionId: string) {
  }
}

export class PasswordReminderRequest {
  public constructor(public email: string) {
  }
}

export class PasswordReminderResponse {
  public constructor(public status: PasswordReminderStatus) {
  }

  static fromJSON(json: any): PasswordReminderResponse {
    let response = Object.create(PasswordReminderResponse.prototype);
    // copy all the fields from the json object
    //noinspection TypeScriptValidateTypes
    return Object.assign(response, json, {
      // convert fields that need converting
      status: PasswordReminderStatus[json.status]
    });
  }
}

export enum PasswordReminderStatus {
  EmailNotFound, TokenExists, MailSent, Error
}

export class ChangePasswordRequest {
  public constructor(public tokenId: string, public newPassword: string) {
  }
}

export class ChangePasswordResponse {
  public constructor(public status: ChangePasswordStatus) {
  }

  static fromJSON(json: any): ChangePasswordResponse {
    let response = Object.create(ChangePasswordResponse.prototype);
    // copy all the fields from the json object
    //noinspection TypeScriptValidateTypes
    return Object.assign(response, json, {
      // convert fields that need converting
      status: ChangePasswordStatus[json.status]
    });
  }
}

export enum ChangePasswordStatus {
  ExpiredToken, InvalidPassword, PasswordChanged}
