import {Contract} from "./contract";
import {DistributionRole, DistributionType} from "./distribution";

export class Cooperator {
  public constructor(public firstName: string,
                     public lastName: string,
                     public email: string,
                     public telephone: string,
                     public address: string,
                     public role: CooperatorRole,
                     public contracts: Contract[],
                     public _id: string,
                     public coordinator: boolean,
                     public additionalCooperators: number) {
  }

  public toCooperatorInfo() : CooperatorInfo {
    return new CooperatorInfo(this._id, this.firstName, this.lastName, this.email, this.telephone );
  }

  public isAdmin() {
    return this.role === CooperatorRole.AmapAdmin || this.role === CooperatorRole.TechAdmin;
  }

  public hasContract(type: string): boolean {
    return this.contracts.some(t => DistributionType[t.contractType] === type);
  }

  public setContract(type: string, contains: boolean) {
    this.contracts = this.contracts.filter(t => t.contractType !== DistributionType[type]);
    if (contains) {
      this.contracts.push(new Contract(DistributionType[type], DistributionRole.Normal));
    }
  }

  public isReferent(contract: string): boolean {
    return this.contracts.some(t =>
      DistributionType[t.contractType] === contract && t.distributionRole === DistributionRole.Referent
    );
  }

  public setReferent(type: string, referent: boolean) {
    const contract = this.contracts.filter(t => t.contractType === DistributionType[type])[0];
    if (contract) {
      contract.distributionRole = referent ? DistributionRole.Referent : DistributionRole.Normal;
    }
  }

  public isReferentCooperator(): boolean {
    return this.contracts.some(c => c.distributionRole === DistributionRole.Referent);
  }

  static fromJSON(json?: any): Cooperator {
    if (!json) {
      return null;
    }
    const cooperator = Object.create(Cooperator.prototype);
    const result = Object.assign(cooperator, json, {
      contracts: Contract.fromJSONArray(json.contracts),
      role: CooperatorRole[json.role]
    });

    return result;
  }

  static fromJSONArray(json?: Array<any>): Cooperator[] {
    return json ? json.map(Cooperator.fromJSON) : [];
  }
}

export enum CooperatorRole {
  Cooperator, AmapAdmin, TechAdmin
}

export class CooperatorParticipation {
  public constructor(
    public cooperatorId: string,
    public distributionId: string,
    public distributionRole: DistributionRole,
    public number:number,
    public present: boolean) {
  }

  static fromJSON(json?: any): CooperatorParticipation {
    if (!json) {
      return null;
    }
    const cooperator = Object.create(CooperatorParticipation.prototype);
    const result = Object.assign(cooperator, json, {
      distributionRole: CooperatorRole[json.distributionRole]
    });

    return result;
  }

  static fromJSONArray(json?: Array<any>): CooperatorParticipation[] {
    return json ? json.map(CooperatorParticipation.fromJSON) : [];
  }
}

export class CooperatorInfo {
  public constructor(public _id: string,
                     public firstName: string, public lastName: string,
                     public email?: string, public telephone?: string) {
  }

  static fromJSON(json?: any): CooperatorInfo {
    if (!json) {
      return null;
    }
    const cooperator = Object.create(CooperatorInfo.prototype);
    const result = Object.assign(cooperator, json, {});

    return result;
  }

  static fromJSONArray(json?: Array<any>): CooperatorInfo[] {
    return json ? json.map(CooperatorInfo.fromJSON) : [];
  }
}

export class SaveCooperatorRequest {
  public constructor(public cooperator: Cooperator,
                     public additionalEMails: string[]) {
  }
}

export class SaveCooperatorResponse {
  public constructor(public success: boolean,
                     public errorMessage?: string) {
  }

  static fromJSON(json?: any): SaveCooperatorResponse {
    if (!json) {
      return null;
    }
    const cooperator = Object.create(SaveCooperatorResponse.prototype);
    const result = Object.assign(cooperator, json, {});

    return result;
  }
}
