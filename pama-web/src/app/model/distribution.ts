import {Cooperator, CooperatorInfo} from "./cooperator";
import {Product} from "./product";
import * as moment from "moment";


export class Distribution {
  public vegetablesAttendance: number;
  public selectedVegetableProductsIds: string[];
  public selectedApplePearProductsIds: string[];

  public constructor(public date: string,
                     public types: DistributionType[],
                     public attendance: DistributionAttendance[],
                     public products: Product[],
                     public _id: string) {
    this.initDistribution();
  }

  private initDistribution() {
    this.calculateVegetablesAttendance();
    this.selectedVegetableProductsIds = this.getVegetableProducts().map(p => p._id);
    this.selectedApplePearProductsIds = this.getAppleBearProducts().map(p => p._id);
  }

  private calculateVegetablesAttendance() {
    this.vegetablesAttendance = this.attendance.filter(
      a => a.distributionType === DistributionType.Vegetables
      && a.role === DistributionRole.Normal).length;
  }

  public dateValue(): Date {
    return moment(this.date).toDate();
  }

  public setDayMonthYear(day, month, year) {
    const date = this.dateValue();
    date.setUTCFullYear(year, month, day);
    this.date = moment(date).format("YYYY-MM-DDTHH:mm:ss.SSS");
  }

  public doNotDisplayBecauseIsInThePast(): boolean {
    return moment(this.date).add(2, "hours") < moment();
  }

  public isInThePast(): boolean {
    return moment(this.date) < moment();
  }

  public getCoordinatorId(): string {
    const result = this.attendance
      .filter(a => a.role === DistributionRole.Coordinator)
      .map(a => a.cooperatorId);
    return result.length == 0 ? null : result[0];
  }

  public getReferentIds(type: DistributionType): string[] {
    return this.attendance
      .filter(a => a.distributionType === type && a.role === DistributionRole.Referent)
      .map(a => a.cooperatorId)
  }

  public couldHaveReferentWith(cooperator: Cooperator) {
    return cooperator.coordinator
      ||
      (cooperator.isReferentCooperator()
      && cooperator.contracts.some(c => c.distributionRole === DistributionRole.Referent
      && this.types.indexOf(c.contractType) !== -1))
  }

  public allowedDistributionRoles(cooperator: Cooperator): AllowedDistributionRole[] {
    const r = [];
    if (cooperator.coordinator) {
      r.push(new AllowedDistributionRole(DistributionRole.Coordinator))
    }
    cooperator.contracts.forEach(c => {
      if (c.distributionRole === DistributionRole.Referent) {
        r.push(new AllowedDistributionRole(DistributionRole.Referent, c.contractType))
      }
    });
    r.push(new AllowedDistributionRole(DistributionRole.Normal, DistributionType.Vegetables));
    return r;
  }

  public updateAttendance(cooperator: Cooperator,
                          add: boolean,
                          nbCooperators: number,
                          role?: AllowedDistributionRole) {
    this.attendance = this.attendance.filter(a => a.cooperatorId !== cooperator._id);
    if (add) {
      for (let i = 0; i < nbCooperators; i++) {
        this.attendance.push(new DistributionAttendance(
          role ? role.type : DistributionType.Vegetables,
          role ? role.role : DistributionRole.Normal,
          cooperator._id,
          true));
      }
    }
    this.initDistribution();
  }

  public hasType(type: string): boolean {
    return this.types.some(t => {
      return DistributionType[t] == type;
    });
  }

  public setType(type: string, contains: boolean) {
    if (contains) {
      this.types.push(DistributionType[type]);
    } else {
      this.types = this.types.filter(t => t != DistributionType[type]);
    }
  }

  public hasVegetableType(): boolean {
    return this.hasType(DistributionType[DistributionType.Vegetables]);
  }

  public hasProduct(): boolean {
    return this.products && this.products.length != 0;
  }

  public hasVegetableProduct(): boolean {
    return this.getVegetableProducts().length != 0;
  }

  public getVegetableProducts(): Product[] {
    return this.products.filter(p => p.type === DistributionType.Vegetables)
  }

  public hasAppleBearProduct(): boolean {
    return this.getAppleBearProducts().length != 0;
  }

  public getAppleBearProducts(): Product[] {
    return this.products.filter(p => p.type === DistributionType.ApplePear)
  }

  public vegetablesListText(): string {
    if (this.hasVegetableProduct()) {
      return this.getVegetableProducts().map(p => p.name).join(", ");
    } else {
      return "La liste des légumes distribués n'est pas encore connue."
    }
  }

  static fromJSON(json?: any): Distribution {
    if (!json) {
      return null;
    }
    // create an instance of the Distribution class
    const distribution = Object.create(Distribution.prototype);
    // copy all the fields from the json object
    //noinspection TypeScriptValidateTypes
    const result = Object.assign(distribution, json, {
      // convert fields that need converting
      types: json.types.map(t => DistributionType[t]),
      attendance: DistributionAttendance.fromJSONArray(json.attendance),
      products: Product.fromJSONArray(json.products)
    });

    result.initDistribution();
    return result;
  }

  static fromJSONArray(json?: Array<any>): Distribution[] {
    return json ? json.map(Distribution.fromJSON) : [];
  }
}

export class DistributionAttendance {
  public constructor(public distributionType: DistributionType,
                     public role: DistributionRole,
                     public cooperatorId: string,
                     public present: boolean) {
  }

  label(): string {
    switch (this.role) {
      case DistributionRole.Coordinator :
        return "Coordinateur";
      case DistributionRole.Referent :
        return "Référent " + distributionTypeLabel(this.distributionType);
      case DistributionRole.Normal :
        return "Distributeur " + distributionTypeLabel(this.distributionType);
    }
  }

  static fromJSON(json?: any): DistributionAttendance {
    if (!json) {
      return null;
    }
    // create an instance of the Distribution class
    const distribution = Object.create(DistributionAttendance.prototype);
    // copy all the fields from the json object
    //noinspection TypeScriptValidateTypes
    const result = Object.assign(distribution, json, {
      // convert fields that need converting
      distributionType: DistributionType[json.distributionType],
      role: DistributionRole[json.role]
    });

    return result;
  }

  static fromJSONArray(json?: Array<any>): DistributionAttendance[] {
    return json ? json.map(DistributionAttendance.fromJSON) : [];
  }
}

export enum DistributionType {
  Vegetables, Bread, Cheese, Egg, Chicken, ApplePear, RedFruit
}

const distributionTypesLabels = ["Légumes", "Pain", "Fromage", "Oeufs", "Poulet", "Pommes et Poires", "Fruits Rouges"];
export const distributionTypeLabel = function (type: DistributionType): string {
  return distributionTypesLabels[type];
};

export enum DistributionRole {

  Normal, Referent, Coordinator
}

export class AllowedDistributionRole {

  public constructor(public role: DistributionRole, public type?: DistributionType) {
  }

  label(): string {
    switch (this.role) {
      case DistributionRole.Coordinator :
        return "Coordinateur";
      case DistributionRole.Referent :
        return "Référent " + distributionTypeLabel(this.type);
      case DistributionRole.Normal :
        return "Distributeur " + distributionTypeLabel(this.type);
    }
  }
}

export class LinkedDistribution {
  public constructor(public distribution: Distribution, public cooperators: CooperatorInfo[],
                     public previous?: string, public next?: string) {
  }

  static fromJSON(json?: any): LinkedDistribution {
    if (!json) {
      return null;
    }
    let distribution = Object.create(LinkedDistribution.prototype);
    return Object.assign(distribution, json, {
      distribution: Distribution.fromJSON(json.distribution),
      cooperators: CooperatorInfo.fromJSONArray(json.cooperators)
    });
  }
}

export class DistributionDetails {

  public constructor(public distribution: Distribution, public cooperators: CooperatorInfo[]) {
  }

  static fromJSON(json?: any): DistributionDetails {
    if (!json) {
      return null;
    }
    let distribution = Object.create(DistributionDetails.prototype);
    return Object.assign(distribution, json, {
      cooperators: CooperatorInfo.fromJSONArray(json.cooperators),
      distribution: Distribution.fromJSON(json.distribution)
    });
  }

}

export class DistributionCalendar {

  public constructor(public distributions: Distribution[], public cooperators: CooperatorInfo[]) {
  }

  static fromJSON(json?: any): DistributionCalendar {
    if (!json) {
      return null;
    }
    let distribution = Object.create(LinkedDistribution.prototype);
    return Object.assign(distribution, json, {
      cooperators: CooperatorInfo.fromJSONArray(json.cooperators),
      distributions: Distribution.fromJSONArray(json.distributions)
    });
  }
}
