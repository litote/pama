import {DistributionType, DistributionRole} from "./distribution";
export class Contract {

  public constructor(public contractType:DistributionType,
                     public distributionRole: DistributionRole) {

  }

  static fromJSON(json?: any): Contract {
    if(!json) {
      return null;
    }
    // create an instance of the Distribution class
    const contract = Object.create(Contract.prototype);
    // copy all the fields from the json object
    //noinspection TypeScriptValidateTypes
    const result = Object.assign(contract, json, {
      // convert fields that need converting
      contractType: DistributionType[json.contractType],
      distributionRole: DistributionRole[json.distributionRole]
    });

    return result;
  }

  static fromJSONArray(json?: Array<any>): Contract[] {
    return json ? json.map(Contract.fromJSON) : [];
  }
}
