import {DistributionType} from "./distribution";
export class Product {

  public value:string;
  public label:string;

  public constructor(public name: string,
                     public _id: string,
                     public type:DistributionType,
                     public image?: string) {
    this.value = _id;
    this.label = name;
  }

  image256(): string {
    if (this.image) return this.image + "_256"; else return "vegetables_256";
  }

  isVegetable(): boolean {
     return this.type === DistributionType.Vegetables;
  }

  isApplePear(): boolean {
    return this.type === DistributionType.ApplePear;
  }

  public setType(type: string, set: boolean) {
    if(set) {
      this.type = DistributionType[type];
    }
  }

  static fromJSON(json: any): Product {
    const product = Object.create(Product.prototype);
    //noinspection TypeScriptValidateTypes
    const result = Object.assign(product, json, {
      value : json._id,
      label : json.name,
      type : DistributionType[json.type]
    });

    return result;
  }

  static fromJSONArray(json: Array<any>): Product[] {
    return json ? json.map(Product.fromJSON) : [];
  }
}

