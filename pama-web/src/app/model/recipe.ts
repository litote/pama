import {Product} from "./product";
export class Recipe {

  public constructor(public productsIds: string[], public _id: string, public title: string,
                     public referenceUrl: string, public durationLabel: string, public ingredientsScope: string,
                     public ingredients: string[], public description: string) {
  }

  getProducts(allProducts:Product[], restricted:string[]) : Product[] {
    const result = [];
    this.productsIds.forEach(id => {
      const filtered = allProducts.filter(p => p._id == id && (!restricted || restricted.length == 0 || restricted.some(id2 => id2 == id)));
      if(filtered.length > 0) {
        result.push(filtered[0])
      }
    });
    return result;
  }

  static fromJSON(json: any): Recipe {
    const recipe = Object.create(Recipe.prototype);
    //noinspection TypeScriptValidateTypes
    const result = Object.assign(recipe, json, {
    });

    return result;
  }

  static fromJSONArray(json: Array<any>): Recipe[] {
    return json ? json.map(Recipe.fromJSON) : [];
  }

}

export class RecipesRequest {

  public constructor(public productsIds: string[]) {
  }

}

export class RecipesResponse {

  public constructor(public productsIds: string[], public recipes: Recipe[]) {
  }

  static fromJSON(json: any): RecipesResponse {
    const response = Object.create(RecipesResponse.prototype);
    //noinspection TypeScriptValidateTypes
    const result = Object.assign(response, json, {
      recipes : Recipe.fromJSONArray(json.recipes)
    });

    return result;
  }

}

