import {AfterViewChecked, ChangeDetectorRef, Component, OnInit} from "@angular/core";
import {ActivatedRoute, Params, Router} from "@angular/router";
import {Recipe} from "../../model/recipe";
import {Product} from "../../model/product";
import {DistributionService} from "../../core/distribution.service";

@Component({
  selector: 'recipe',
  templateUrl: './recipe.component.html',
  styleUrls: ['../../commons.css', './recipe.component.css']
})
export class RecipeComponent implements OnInit, AfterViewChecked {

  recipe: Recipe;
  allProducts: Product[];

  constructor(private route: ActivatedRoute,
              private router: Router,
              public distributionService: DistributionService,
              ///TODO remove AFterViewChecked
              private cdRef: ChangeDetectorRef) {
  }

  ngAfterViewChecked() {
    //explicit change detection to avoid "expression-has-changed-after-it-was-checked-error"
    this.cdRef.detectChanges();
  }

  ngOnInit() {
    this.distributionService.getProducts().subscribe(
      products => this.allProducts = products,
      error => window.alert(error));

    this.route.params.forEach((params: Params) => {
      const id = params['id'];

      this.distributionService.getRecipe(id).subscribe(
        recipe => this.recipe = recipe,
        error => window.alert(error)
      );
    });
  }


}
