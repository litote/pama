import {Routes, RouterModule} from "@angular/router";
import {RecipesComponent} from "./recipes/recipes.component";
import {RecipeComponent} from "./recipe/recipe.component";
import {NgModule} from "@angular/core";

const recipeRoutes: Routes = [
  {
    path: '',
    component: RecipesComponent
  },
  {
    path: ':id',
    component: RecipeComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(recipeRoutes)],
  exports: [RouterModule]
})
export class RecipeRoutingModule {
}
