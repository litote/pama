import { Component, OnInit } from '@angular/core';
import {Params, Router, ActivatedRoute} from "@angular/router";
import {Product} from "../../model/product";
import {Recipe} from "../../model/recipe";
import {DistributionService} from "../../core/distribution.service";
import {DistributionType} from "../../model/distribution";

@Component({
  selector: 'recipes',
  templateUrl: './recipes.component.html',
  styleUrls: ['../../commons.css', './recipes.component.css']
})
export class RecipesComponent implements OnInit {

  allProducts: Product[];
  productsIds: string[];
  recipes: Recipe[];

  constructor(private route: ActivatedRoute,
              private router: Router,
              public distributionService: DistributionService) {
  }

  ngOnInit() {
    this.distributionService.getProducts().subscribe(
      products => this.allProducts = products.filter(p => p.type == DistributionType.Vegetables),
      error => window.alert(error));

    this.route.queryParams.forEach((params: Params) => {
      const id = params['productId'];
      if (id) {
        this.productsIds = [id];
        this.distributionService.getRecipes(this.productsIds).subscribe(
          response => this.recipes = response.recipes,
          error => window.alert(error)
        );
      } else {
        const distributionId = params['distributionId'];
        if (distributionId) {
          this.distributionService.getRecommendedRecipes(distributionId).subscribe(
            response => {
              this.productsIds = response.productsIds;
              this.recipes = response.recipes;
            },
            error => window.alert(error)
          );
        }  else {
          this.productsIds = [];
        }
      }
    });
  }

  onSubmit() {
    let ids = this.productsIds;
    this.distributionService.getRecipes(this.productsIds).subscribe(
      response => {
        this.productsIds = response.productsIds;
        this.recipes = response.recipes;
        console.log(this.recipes);
      },
      error => window.alert(error)
    );

    return false;
  }

  selected(id: string) {
    //do nothing
  }

  deselected(id: string) {
    //do nothing
  }

}

