import {NgModule} from "@angular/core";
import {SharedModule} from "../shared/shared.module";
import {RecipesComponent} from "./recipes/recipes.component";
import {RecipeRoutingModule} from "./recipe-routing.module";
import {RecipeComponent} from "./recipe/recipe.component";
import {SelectModule} from "angular2-select";

@NgModule({
  imports: [
    SharedModule,
    RecipeRoutingModule,
    SelectModule
  ],
  declarations: [
    RecipeComponent,
    RecipesComponent
  ],
  providers: []
})
export class RecipeModule {
}
