import {NgModule} from "@angular/core";
import {HttpModule} from "@angular/http";
import {LoginComponent} from "./login/login.component";
import {AuthRoutingModule} from "./auth-routing.module";
import {AuthGuard} from "./auth.guard";
import {AuthService} from "./auth.service";
import {PasswordReminderComponent} from "./password-reminder/password-reminder.component";
import {ChangePasswordComponent} from "./change-password/change-password.component";
import {SharedModule} from "../../shared/shared.module";

@NgModule({
  imports: [
    SharedModule,
    HttpModule,
    AuthRoutingModule
  ],
  declarations: [
    LoginComponent,
    PasswordReminderComponent,
    ChangePasswordComponent
  ],
  providers: [
    AuthService, AuthGuard
  ]
})
export class AuthModule {
}
