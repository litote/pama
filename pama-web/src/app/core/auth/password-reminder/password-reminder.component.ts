import {OnInit, Component, AfterViewInit, ViewChild} from "@angular/core";
import {AuthService} from "../auth.service";
import {Router} from "@angular/router";
import {MdInputContainer} from "@angular/material";
import {PasswordReminderStatus} from "../../../model/auth";
@Component({
  selector: 'password-reminder',
  templateUrl: './password-reminder.component.html',
  styleUrls: ['./password-reminder.component.css']
})
export class PasswordReminderComponent implements OnInit, AfterViewInit {

  email: string;
  errorMessage: string;
  message: string;
  sendMessage: boolean;

  @ViewChild('emailInputContainer') emailInput: MdInputContainer;

  constructor(private authService: AuthService, private router: Router) {
  }

  ngOnInit(): void {
  }


  ngAfterViewInit(): void {
    this.emailInput._focusInput();
  }

  onSubmit() {
    this.errorMessage = null;
    this.message = null;
    this.sendMessage = true;

    this.authService.passwordReminder(this.email).subscribe(
      response => {
        this.sendMessage = false;
        switch (response.status) {
          case PasswordReminderStatus.EmailNotFound :
            this.errorMessage = "Nous n'avons pas trouvé d'utilisateur correspondant à cet e-mail.";
            break;
          case PasswordReminderStatus.TokenExists :
            this.errorMessage = "Vous avez déjà demandé la réinitialisation du mot de passe. Veuillez consulter le mail reçu, ou réessayez demain.";
            break;
          case PasswordReminderStatus.MailSent :
            this.message = "Un e-mail vous a été envoyé. Merci de le consulter pour modifier votre mot de passe.";
            break;
          default :
            this.errorMessage = "Une erreur imprévue est survenue. Merci de réessayer un peu plus tard.";
        }
      },
      error => {
        this.sendMessage = false;
        this.errorMessage = "Une erreur imprévue est survenue. Merci de réessayer un peu plus tard.";
      }
    );

    return false;
  }

  cancel() {
    //TODO previous url
    this.router.navigateByUrl("/");
  }

}
