import {Component, OnInit, ViewChild, AfterViewInit} from "@angular/core";
import {MdInputContainer} from "@angular/material";
import {AuthService} from "../auth.service";
import {Router} from "@angular/router";

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, AfterViewInit {

  email: string;
  password: string;
  errorMessage: string;
  sendLogin: boolean;

  @ViewChild('emailContainer') emailInput: MdInputContainer;


  constructor(private authService: AuthService, private router: Router) {
  }

  ngOnInit(): void {
  }


  ngAfterViewInit(): void {
    this.emailInput._focusInput();
  }

  onSubmit() {
    this.errorMessage = null;
    this.sendLogin = true;
    this.authService.authenticate(this.email, this.password).subscribe(
      response => {
        this.sendLogin = false;
        if (response && response.sessionId) {
          this.authService.login(response);
          this.router.navigateByUrl(this.authService.getRedirectUrl());
        } else {
          this.errorMessage = "Identifiants incorrects";
        }
      },
      error => {
        this.sendLogin = false;
      }
    );
    return false;
  }

  cancel() {
    //TODO previous url
    this.router.navigateByUrl("/");
  }
}
