import {Routes, RouterModule} from "@angular/router";
import {NgModule} from "@angular/core";
import {LoginComponent} from "./login/login.component";
import {PasswordReminderComponent} from "./password-reminder/password-reminder.component";
import {ChangePasswordComponent} from "./change-password/change-password.component";

const authRoutes: Routes = [
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'mot-de-passe-oublie',
    component: PasswordReminderComponent
  } ,
  {
    path: 'modifier-mot-de-passe/:tokenId',
    component: ChangePasswordComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(authRoutes)],
  exports: [RouterModule]
})
export class AuthRoutingModule {
}
