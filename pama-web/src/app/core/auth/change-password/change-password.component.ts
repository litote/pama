import {Component, OnInit, AfterViewInit, ViewChild} from "@angular/core";
import {AuthService} from "../auth.service";
import {Router, ActivatedRoute} from "@angular/router";
import {ChangePasswordStatus} from "../../../model/auth";
import {MdInputContainer} from "@angular/material";

export enum ChangePasswordErrorStatus {
  InvalidToken
}

@Component({
  selector: 'change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit, AfterViewInit {


  password: string;
  checkPassword: string;
  errorMessage: string;
  errorStatus: ChangePasswordErrorStatus;
  sendChangePassword: boolean;
  message: string;
  private tokenId: string;

  @ViewChild('passwordInputContainer') passwordInput: MdInputContainer;

  constructor(private authService: AuthService,
              private route: ActivatedRoute,
              private router: Router) {
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.tokenId = params['tokenId'];
      this.authService.isPasswordReminderTokenValid(this.tokenId).subscribe(valid => {
        if (!valid) {
          this.errorMessage = "Veuillez demander de nouveau une modification de mot de passe.";
          this.errorStatus = ChangePasswordErrorStatus.InvalidToken
        }
      });
    });
  }


  ngAfterViewInit(): void {
    this.passwordInput._focusInput();
  }

  onSubmit() {
    this.errorMessage = null;
    this.message = null;

    if (this.password != this.checkPassword) {
      this.errorMessage = "Les mots de passe ne correspondent pas.";
      return;
    }
    if (this.password.length < 8) {
      this.errorMessage = "Le mot de passe doit comprendre au moins 8 caractères";
      return;
    }
    this.sendChangePassword = true;
    this.authService.changePassword(this.tokenId, this.password).subscribe(
      response => {
        this.sendChangePassword = false;
        switch (response.status) {
          case ChangePasswordStatus.ExpiredToken :
            this.errorMessage = "La date d'expiration du lien de modification de mot de passe a expiré.";
            break;
          case ChangePasswordStatus.InvalidPassword:
            this.errorMessage = "Mot de passe non valide (au moins 8 caractères, avec au minimum une majuscule, une minuscule, un chiffre et un caractère spécial).";
            break;
          case ChangePasswordStatus.PasswordChanged:
            this.message = "Votre mot de passe a bien été modifié.";
            break;
        }
      },
      error => {
        this.sendChangePassword = false;
      }
    );
    return false;
  }

}
