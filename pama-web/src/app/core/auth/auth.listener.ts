import {Cooperator} from "../../model/cooperator";

export interface AuthListener {
  login(cooperator: Cooperator)
  logout()
}
