import {Injectable} from "@angular/core";
import {Observable} from "rxjs/Observable";
import {Http, Response} from "@angular/http";
import {environment} from "../../../environments/environment";
import {
  PasswordReminderResponse,
  PasswordReminderRequest,
  ChangePasswordResponse,
  ChangePasswordRequest,
  AuthenticateResponse,
  AuthenticateRequest,
  LogoutRequest,
  SessionResponse,
  SessionRequest
} from "../../model/auth";
import {AuthListener} from "./auth.listener";
import {Router} from "@angular/router";

@Injectable()
export class AuthService {

  private static SESSION_ID_KEY = "sessionId";

  private sessionId: string;
  private redirectUrl: string;
  private authListeners: AuthListener[] = [];
  private loading: boolean;

  constructor(private http: Http, private router: Router) {
    const storedId = this.getStoredSessionId();
    if (storedId) {
      this.loading = true;
      this.checkSession(storedId)
        .subscribe(
          response => {
            this.loading = false;
            if (response.cooperator) {
              this.login(response);
              if (this.redirectUrl) {
                this.router.navigateByUrl(this.redirectUrl);
              }
            } else {
              this.logout();
            }
          },
          err => this.loading = false);
    }
  }

  isLoading(): boolean {
    return this.loading;
  }

  private getStoredSessionId(): string {
    return localStorage.getItem(AuthService.SESSION_ID_KEY);
  }

  private storeSessionId() {
    localStorage.setItem(AuthService.SESSION_ID_KEY, this.sessionId);
  }

  private removeStoredSessionId() {
    localStorage.removeItem(AuthService.SESSION_ID_KEY);
  }

  setRedirectUrl(redirectUrl: string) {
    this.redirectUrl = redirectUrl;
  }

  getRedirectUrl() {
    return this.redirectUrl ? this.redirectUrl : "/";
  }

  isLoggedIn(): boolean {
    return this.sessionId != null
  }

  addListener(listener: AuthListener) {
    this.authListeners.push(listener);
  }

  login(response: AuthenticateResponse) {
    this.sessionId = response.sessionId;
    this.storeSessionId();
    this.authListeners.forEach(l => l.login(response.cooperator));
  }

  logout() {
    this.removeStoredSessionId();
    this.http.post(`${environment.serverUrl}/logout`, new LogoutRequest(this.sessionId))
      .subscribe(response => {
        this.sessionId = null;
        this.authListeners.forEach(l => l.logout());
        this.router.navigateByUrl("/");
      }, AuthService.handleError);
  }

  passwordReminder(email: string): Observable<PasswordReminderResponse> {
    return this.http.post(`${environment.serverUrl}/password-reminder`, new PasswordReminderRequest(email))
      .map(AuthService.extractPasswordReminderResponse)
      .catch(AuthService.handleError);
  }

  isPasswordReminderTokenValid(tokenId: string): Observable<boolean> {
    return this.http.get(`${environment.serverUrl}/password-reminder-token-valid/${tokenId}`)
      .map(res => (res.json() || {}).valid)
      .catch(AuthService.handleError);
  }

  changePassword(tokenId: string, newPassword: string): Observable<ChangePasswordResponse> {
    return this.http.post(`${environment.serverUrl}/change-password`, new ChangePasswordRequest(tokenId, newPassword))
      .map(AuthService.extractChangePasswordResponse)
      .catch(AuthService.handleError);
  }

  authenticate(email: string, password: string): Observable<AuthenticateResponse> {
    return this.http.post(`${environment.serverUrl}/authenticate`, new AuthenticateRequest(email, password))
      .map(AuthService.extractAuthenticationResponse)
      .catch(AuthService.handleError);
  }

  private checkSession(sessionId: string): Observable<SessionResponse> {
    return this.http.post(`${environment.serverUrl}/checkSession`, new SessionRequest(sessionId))
      .map(AuthService.extractSessionResponse)
      .catch(AuthService.handleError);
  }

  private static extractSessionResponse(res: Response): SessionResponse {
    return SessionResponse.fromJSON(res.json() || {});
  }

  private static extractAuthenticationResponse(res: Response): AuthenticateResponse {
    return AuthenticateResponse.fromJSON(res.json() || {});
  }

  private static extractPasswordReminderResponse(res: Response): PasswordReminderResponse {
    return PasswordReminderResponse.fromJSON(res.json() || {});
  }

  private static extractChangePasswordResponse(res: Response): ChangePasswordResponse {
    return ChangePasswordResponse.fromJSON(res.json() || {});
  }

  private static handleError(error: Response | any) {
    return Observable.throw("Une erreur est survenue");
  }
}
