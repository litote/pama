import {Injectable} from "@angular/core";
import {Http, Response} from "@angular/http";
import {Observable} from "rxjs/Observable";
import {LinkedDistribution, DistributionDetails, Distribution, DistributionCalendar} from "../model/distribution";
import {environment} from "../../environments/environment";
import {Cooperator, CooperatorParticipation} from "../model/cooperator";
import {Product} from "../model/product";
import {Recipe, RecipesRequest, RecipesResponse} from "../model/recipe";
import {AdminService} from "../admin/admin.service";

@Injectable()
export class DistributionService {

  constructor(private http: Http) {
  }

  getDistributionCalendar(): Observable<DistributionCalendar> {
    return this.http.get(`${environment.serverUrl}/distributions/calendar`)
      .map(DistributionService.extractDistributionCalendar)
      .catch(this.handleError);
  }

  getCurrentDistribution(): Observable<LinkedDistribution> {
    return this.http.get(`${environment.serverUrl}/currentDistribution`)
      .map(DistributionService.extractDisplayedDistribution)
      .catch(this.handleError);
  }

  getDistribution(id: string): Observable<LinkedDistribution> {
    return this.http.get(`${environment.serverUrl}/distribution/${id}`)
      .map(DistributionService.extractDisplayedDistribution)
      .catch(this.handleError);
  }

  getDistributionDetails(id: string): Observable<DistributionDetails> {
    return this.http.get(`${environment.serverUrl}/distribution/details/${id}`)
      .map(DistributionService.extractDistributionDetails)
      .catch(this.handleError);
  }

  getParticipations(id: string): Observable<CooperatorParticipation[]> {
    return this.http.get(`${environment.serverUrl}/participations/${id}`)
      .map(DistributionService.extractCooperatorParticipations)
      .catch(this.handleError);
  }

  getProducts(): Observable<Product[]> {
    return this.http.get(`${environment.serverUrl}/products`)
      .map(DistributionService.extractArrayProducts)
      .catch(this.handleError);
  }

  postParticipation(participation: CooperatorParticipation): Observable<Boolean> {
    return this.http.post(`${environment.serverUrl}/participation`, JSON.stringify(participation))
      .map(DistributionService.extractNoData)
      .catch(this.handleError);
  }

  updateParticipation(participation: CooperatorParticipation): Observable<Boolean> {
    return this.http.post(`${environment.serverUrl}/admin/participation`, JSON.stringify(participation))
      .map(DistributionService.extractNoData)
      .catch(this.handleError);
  }

  getRecommendedRecipes(distributionId: string): Observable<RecipesResponse> {
    return this.http.get(`${environment.serverUrl}/recipes/${distributionId}`)
      .map(DistributionService.extractRecipesResponse)
      .catch(this.handleError);
  }

  getRecipe(id: string): Observable<Recipe> {
    return this.http.get(`${environment.serverUrl}/recipe/${id}`)
      .map(DistributionService.extractRecipe)
      .catch(this.handleError);
  }

  getRecipes(productsIds: string[]): Observable<RecipesResponse> {
    let body = new RecipesRequest(productsIds);
    //TODO understand why this is necessary
    if (productsIds.length == 0) {
      body = {productsIds: []}
    }
    return this.http.post(`${environment.serverUrl}/recipes`, body)
      .map(DistributionService.extractRecipesResponse)
      .catch(this.handleError);
  }

  //TODO duplicate
  getCooperators(): Observable<Cooperator[]> {
    return this.http.get(`${environment.serverUrl}/admin/cooperators`)
      .map(DistributionService.extractArrayCooperators)
      .catch(this.handleError);
  }

  private static extractArrayCooperators(res: Response): Cooperator[] {
    return Cooperator.fromJSONArray(res.json() || []);
  }

  private static extractArrayData(res: Response) {
    return res.json() || [];
  }

  private static extractData(res: Response) {
    return res.json() || {};
  }

  private static extractRecipe(res: Response) {
    return Recipe.fromJSON(res.json() || {});
  }

  private static extractRecipesResponse(res: Response) {
    return RecipesResponse.fromJSON(res.json() || {});
  }

  private static extractArrayProducts(res: Response): Product[] {
    return Product.fromJSONArray(res.json() || []);
  }

  private static extractDistributionDetails(res: Response): DistributionDetails {
    return DistributionDetails.fromJSON(res.json() || {});
  }

  private static extractCooperatorParticipations(res: Response): CooperatorParticipation[] {
    return CooperatorParticipation.fromJSONArray(res.json() || []);
  }

  private static extractDisplayedDistribution(res: Response): LinkedDistribution {
    return LinkedDistribution.fromJSON(res.json() || {});
  }

  private static extractDistributionCalendar(res: Response): DistributionCalendar {
    return DistributionCalendar.fromJSON(res.json() || {});
  }

  private static extractNoData(res: Response) {
    return res.ok;
  }

  private handleError(error: Response | any) {
    // In a real world app, we might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      errMsg = `${error.status} - ${error.statusText || ''}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }
}
