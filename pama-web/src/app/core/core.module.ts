import {NgModule, SkipSelf, Optional} from "@angular/core";
import {HttpModule} from "@angular/http";
import {DistributionService} from "./distribution.service";
import {AuthModule} from "./auth/auth.module";
import {LoggedCooperator} from "./logged-cooperator";

@NgModule({
  imports: [HttpModule, AuthModule],
  declarations: [],
  exports: [],
  providers: [DistributionService, LoggedCooperator]
})
export class CoreModule {

  constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
    if (parentModule) {
      throw new Error(
        'CoreModule is already loaded. Import it in the AppModule only');
    }
  }

}
