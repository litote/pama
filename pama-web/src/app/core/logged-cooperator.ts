import {Injectable} from "@angular/core";
import {Cooperator, CooperatorParticipation} from "../model/cooperator";
import {AuthListener} from "./auth/auth.listener";
import {DistributionService} from "./distribution.service";
import {AuthService} from "./auth/auth.service";
import {Observable} from "rxjs";

@Injectable()
export class LoggedCooperator implements AuthListener {

  public cooperator: Cooperator = null;
  public participations: CooperatorParticipation[] = [];
  private logged: boolean = false;

  constructor(private authService: AuthService, private distributionService: DistributionService) {
    this.authService.addListener(this);
  }

  login(cooperator: Cooperator) {
    this.cooperator = cooperator;
    this.distributionService.getParticipations(cooperator._id)
      .subscribe(
        participations => {
          this.participations = participations;
          this.logged = true;
        },
        error => window.alert(error)
      )
  }

  logout() {
    this.logged = false;
    this.cooperator = null;
    this.participations = [];
  }

  public isAdmin() {
    return this.isLogged() && this.cooperator.isAdmin();
  }

  public isLogged(): boolean {
    return this.logged;
  }

  hasMadeChoice(distributionId: string): boolean {
    return this.participations.some(p => p.distributionId === distributionId);
  }

  participate(distributionId: string): boolean {
    return this.participations.some(p => p.present && p.distributionId === distributionId);
  }

  addParticipation(participation: CooperatorParticipation): Observable<Boolean> {
    this.participations = this.participations
      .filter(p => !(p.distributionId === participation.distributionId && p.cooperatorId === participation.cooperatorId));
    this.participations.push(participation);
    return this.distributionService.postParticipation(participation);
  }

}
