import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {HomeComponent} from "./home.component";
import {DistributionDetailsComponent} from "./distribution-details.component";
import {NextDistributionsComponent} from "./next-distributions.component";

const routes: Routes = [
  {path: '', redirectTo: '/panier', pathMatch: 'full'},
  {
    path: 'panier',
    component: HomeComponent
  },
  {
    path: 'panier/details/:id',
    component: DistributionDetailsComponent
  },
  {
    path: 'calendrier',
    component: NextDistributionsComponent
  },
  {
    path: 'admin',
    loadChildren: 'app/admin/admin.module#AdminModule'
  },
  {
    path: 'recette',
    loadChildren: 'app/recipe/recipe.module#RecipeModule'
  },
  {
    path: 'mon-compte',
    loadChildren: 'app/my-account/my-account.module#MyAccountModule'
  }
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
