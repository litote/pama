import {Component, OnInit} from "@angular/core";
import {LoggedCooperator} from "./core/logged-cooperator";
import {AuthService} from "./core/auth/auth.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./commons.css', './app.component.css']
})
export class AppComponent implements OnInit {

  constructor(public loggedCooperator: LoggedCooperator, private authService: AuthService) {
  }

  ngOnInit() {
    const id = document.getElementById("initialized");
    if (id) {
      id.id = "initialized_ok";
    }
  }

  logout() {
    this.authService.logout();
  }

}
