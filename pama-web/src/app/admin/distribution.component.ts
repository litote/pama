import {AfterViewChecked, ChangeDetectorRef, Component, OnInit} from "@angular/core";
import {AdminService} from "./admin.service";
import {ActivatedRoute, Params, Router} from "@angular/router";
import {Distribution, DistributionType} from "../model/distribution";
import {Product} from "../model/product";
import {DistributionService} from "../core/distribution.service";
import {MdDialog} from "@angular/material";
import {ConfirmDialogComponent} from "../shared/confirm-dialog/confirm-dialog.component";

@Component({
  selector: 'distribution',
  templateUrl: './distribution.component.html',
  styleUrls: ['./distribution.component.css']
})
export class DistributionComponent implements OnInit, AfterViewChecked {
  distribution: Distribution = new Distribution("2017-01-04T18:30:00", [DistributionType.Vegetables], [], [], "");
  allProducts: Product[];
  allVegetableProducts: Product[];
  allApplePearProducts: Product[];

  constructor(private route: ActivatedRoute, private router: Router,
              private adminService: AdminService, private distributionService: DistributionService,
              private dialog: MdDialog,
              ///TODO remove AFterViewChecked
              private cdRef: ChangeDetectorRef) {
  }

  ngAfterViewChecked() {
    //explicit change detection to avoid "expression-has-changed-after-it-was-checked-error"
    this.cdRef.detectChanges();
  }

  ngOnInit() {
    this.route.params.forEach((params: Params) => {
      const id = params['id'];
      if (id != "new") {
        this.adminService.getDistribution(params['id']).subscribe(
          distribution => {
            this.distribution = Distribution.fromJSON(distribution);
            //this.firstName.focus();
            this.distributionService.getProducts().subscribe(
              products => {
                this.allProducts = products;
                this.allVegetableProducts = products.filter(p => p.type === DistributionType.Vegetables);
                this.allApplePearProducts = products.filter(p => p.type === DistributionType.ApplePear);
              },
              error => window.alert(error)
            )
          },
          error => window.alert(error)
        )
      }
    });
  }

  onDateChanged(event) {
    this.distribution.setDayMonthYear(event.date.day, event.date.month - 1, event.date.year);
  }

  cancel() {
    this.router.navigate(["/admin"]);
    return false;
  }

  delete() {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title: "Supprimer la distribution",
        subtitle: "Etes-vous sûr?",
        action: "Confirmer"
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result === "confirm") {
        this.adminService.deleteDistribution(this.distribution).subscribe(
          () => this.router.navigate(["/admin"]),
          error => window.alert(error));
      }
    });
    return false;
  }

  onSubmit() {
    this.adminService.saveDistribution(this.distribution).subscribe(
      () => this.router.navigate(["/admin"]),
      error => window.alert(error));

    return false;
  }

  selected(id: string) {
    this.distribution.products.push(this.allProducts.filter(p => p._id == id)[0]);
  }

  deselected(id: string) {
    this.distribution.products = this.distribution.products.filter(p => p._id != id);
  }

}
