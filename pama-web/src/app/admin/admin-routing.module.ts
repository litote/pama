import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {UserEditComponent} from "./user-edit.component";
import {DistributionComponent} from "./distribution.component";
import {AdminDashboardComponent} from "./admin-dashboard.component";
import {AuthGuard} from "../core/auth/auth.guard";
import {AdminGuard} from "./admin.guard";
import {AlertsComponent} from "./alerts.component";
import {ProductEditComponent} from "./product-edit/product-edit.component";

const adminRoutes: Routes = [
  {
    path: '',
    canActivate: [AuthGuard, AdminGuard],
    component: AdminDashboardComponent,
  },
  {
    path: 'cooperator/:id',
    canActivate: [AuthGuard, AdminGuard],
    component: UserEditComponent,
  },
  {
    path: 'distribution/:id',
    canActivate: [AuthGuard, AdminGuard],
    component: DistributionComponent,
  },
  {
    path: 'product/:id',
    canActivate: [AuthGuard, AdminGuard],
    component: ProductEditComponent,
  },
  {
    path: 'alerts',
    canActivate: [AuthGuard, AdminGuard],
    component: AlertsComponent,
  }
  ];


@NgModule({
  imports: [RouterModule.forChild(adminRoutes)],
  exports: [RouterModule]
})
export class AdminRoutingModule {
}
