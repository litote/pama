import {Component, OnInit, ViewChild} from "@angular/core";
import {Cooperator, CooperatorParticipation, CooperatorRole, SaveCooperatorRequest} from "../model/cooperator";
import {AdminService} from "./admin.service";
import {ActivatedRoute, Params, Router} from "@angular/router";
import {MdDialog, MdInputContainer} from "@angular/material";
import {DistributionType} from "../model/distribution";
import {DistributionService} from "../core/distribution.service";
import {LoggedCooperator} from "../core/logged-cooperator";
import {ConfirmDialogComponent} from "app/shared/confirm-dialog/confirm-dialog.component";

@Component({
  selector: 'user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.css']
})
export class UserEditComponent implements OnInit {
  cooperator: Cooperator = new Cooperator("", "", "", "", "", CooperatorRole.Cooperator, [], undefined, false, 0);
  distributionTypes: DistributionType[] = [
    DistributionType.Vegetables, DistributionType.Bread, DistributionType.Cheese,
    DistributionType.Egg, DistributionType.Chicken, DistributionType.ApplePear,
    DistributionType.RedFruit];
  participations: CooperatorParticipation[] = [];

  saveError: string;

  requestPasswordCreationRequest: boolean;
  requestPasswordCreationMessage: string;
  requestPasswordCreationError: string;

  @ViewChild('firstNameContainer') firstName: MdInputContainer;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private loggedCooperator: LoggedCooperator,
              private adminService: AdminService,
              private distributionService: DistributionService,
              private dialog: MdDialog) {
  }

  ngOnInit() {
    this.route.params.forEach((params: Params) => {

      const param = params['id'];
      if (param === "new") {
        //do nothing
      } else {
        this.adminService.getCooperator(param).subscribe(
          cooperator => {
            this.cooperator = cooperator;
            this.firstName._focusInput();
          },
          error => window.alert(error)
        );
        this.distributionService.getParticipations(param).subscribe(
          participations => this.participations = participations.filter(p => p.present),
          error => window.alert(error)
        );
      }
    });

  }

  getRole(r: string): CooperatorRole {
    return CooperatorRole[r];
  }

  cancel(): boolean {
    this.router.navigateByUrl("/admin?tab=cooperators");
    return false;
  }

  deleteUser() {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title: "Supprimer l'utilisateur",
        subtitle: "Etes-vous sûr?",
        action: "Confirmer"
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result === "confirm") {
        this.adminService.deleteCooperator(this.cooperator).subscribe(
          status => {
            this.cancel();
          },
          error => {
            window.alert(error)
          }
        );
      }
    });
    return false;
  }

  onSubmit(): boolean {
    this.saveError = null;
    const request = new SaveCooperatorRequest(this.cooperator, []);
    this.adminService.saveCooperator(request).subscribe(
      response => {
        if (response.success) {
          if (this.cooperator._id === this.loggedCooperator.cooperator._id) {
            this.adminService.getCooperator(this.cooperator._id).subscribe(
              c => {
                this.loggedCooperator.login(c);
                this.router.navigateByUrl("/admin?tab=cooperators");
              }
            );

          } else {
            this.router.navigateByUrl("/admin?tab=cooperators");
          }
        } else {
          this.saveError = response.errorMessage;
        }

      },
      error => window.alert(error)
    );

    return false;
  }

  requestPasswordCreation(): boolean {
    this.requestPasswordCreationError = null;
    this.requestPasswordCreationMessage = null;

    if (this.cooperator.email.trim().length == 0) {
      this.requestPasswordCreationError = "Veuillez tout d'abord indiquer un email.";
    }
    this.requestPasswordCreationRequest = true;
    this.adminService.requestPasswordCreation(this.cooperator).subscribe(
      () => {
        this.requestPasswordCreationRequest = false;
        this.requestPasswordCreationMessage = "Le mail a été envoyé."
      },
      error => {
        this.requestPasswordCreationRequest = false;
        this.requestPasswordCreationError = "Une erreur s'est produite. Veuillez réessayer un peu plus tard.";
      }
    );
    return false;
  }
}
