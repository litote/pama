import {Injectable} from "@angular/core";
import {Http, Response} from "@angular/http";
import {Observable} from "rxjs/Observable";
import {Cooperator, SaveCooperatorRequest, SaveCooperatorResponse} from "../model/cooperator";
import {environment} from "../../environments/environment";
import {Distribution} from "../model/distribution";
import {Product} from "../model/product";
import {Alert, TestAlertRequest} from "../model/alerts";
@Injectable()
export class AdminService {

  constructor(private http: Http) {
  }

  getCooperators(): Observable<Cooperator[]> {
    return this.http.get(`${environment.serverUrl}/admin/cooperators`)
      .map(AdminService.extractArrayCooperators)
      .catch(this.handleError);
  }

  getCooperator(id: string): Observable<Cooperator> {
    return this.http.get(`${environment.serverUrl}/admin/cooperator/${id}`)
      .map(AdminService.extractCooperator)
      .catch(this.handleError);
  }

  saveCooperator(request: SaveCooperatorRequest): Observable<SaveCooperatorResponse> {
    return this.http.post(`${environment.serverUrl}/admin/cooperator`, JSON.stringify(request))
      .map(AdminService.extractSaveCooperatorResponse)
      .catch(this.handleError);
  }

  deleteCooperator(cooperator: Cooperator): Observable<boolean> {
    return this.http.delete(`${environment.serverUrl}/admin/cooperator/${cooperator._id}`)
      .map(this.extractNoData)
      .catch(this.handleError);
  }

  getDistributions(): Observable<Distribution[]> {
    return this.http.get(`${environment.serverUrl}/admin/distributions`)
      .map(this.extractArrayData)
      .catch(this.handleError);
  }

  getDistribution(id: string): Observable<Distribution> {
    return this.http.get(`${environment.serverUrl}/admin/distribution/${id}`)
      .map(this.extractData)
      .catch(this.handleError);
  }

  saveDistribution(distribution: Distribution): Observable<boolean> {
    return this.http.post(`${environment.serverUrl}/admin/distribution`, JSON.stringify(distribution))
      .map(this.extractNoData)
      .catch(this.handleError);
  }

  deleteDistribution(distribution: Distribution): Observable<boolean> {
    return this.http.delete(`${environment.serverUrl}/admin/distribution/${distribution._id}`)
      .map(this.extractNoData)
      .catch(this.handleError);
  }

  saveProduct(product: Product): Observable<boolean> {
    return this.http.post(`${environment.serverUrl}/admin/product`, JSON.stringify(product))
      .map(this.extractNoData)
      .catch(this.handleError);
  }

  deleteProduct(product: Product): Observable<boolean> {
    return this.http.delete(`${environment.serverUrl}/admin/product/${product._id}`)
      .map(this.extractNoData)
      .catch(this.handleError);
  }

  getProduct(productId: string): Observable<Product> {
    return this.http.get(`${environment.serverUrl}/admin/product/${productId}`)
      .map(this.extractData)
      .map(Product.fromJSON)
      .catch(this.handleError);
  }

  getProducts(): Observable<Product[]> {
    return this.http.get(`${environment.serverUrl}/admin/products`)
      .map(this.extractArrayData)
      .map(Product.fromJSONArray)
      .catch(this.handleError);
  }

  requestPasswordCreation(cooperator: Cooperator) {
    return this.http.post(`${environment.serverUrl}/admin/cooperator/requestPasswordCreation`, JSON.stringify(cooperator))
      .map(this.extractNoData)
      .catch(this.handleError);
  }

  testAlert(test: TestAlertRequest): Observable<boolean> {
    return this.http.post(`${environment.serverUrl}/admin/alerts/testAlert`, JSON.stringify(test))
      .map(this.extractNoData)
      .catch(this.handleError);
  }

  sendPasswordDefinitionAlert() {
    return this.http.post(`${environment.serverUrl}/admin/alerts/sendPasswordDefinitionAlert`, "{}")
      .map(this.extractNoData)
      .catch(this.handleError);
  }

  saveAlert(alert: Alert): Observable<boolean> {
    return this.http.post(`${environment.serverUrl}/admin/alert`, JSON.stringify(alert))
      .map(this.extractNoData)
      .catch(this.handleError);
  }

  getAlert(alertType: string): Observable<Alert> {
    return this.http.get(`${environment.serverUrl}/admin/alert/${alertType}`)
      .map(this.extractData)
      .map(Alert.fromJSON)
      .catch(this.handleError);
  }

  getCooperatorsWithLessParticipation(): Observable<any> {
    return this.http.get(`${environment.serverUrl}/admin/cooperators/lessparticipation`)
      .map(this.extractData)
      .catch(this.handleError);
  }

  private static extractArrayCooperators(res: Response): Cooperator[] {
    return Cooperator.fromJSONArray(res.json() || []);
  }

  private static extractCooperator(res: Response): Cooperator {
    return Cooperator.fromJSON(res.json() || {});
  }

  private static extractSaveCooperatorResponse(res: Response): SaveCooperatorResponse {
    return SaveCooperatorResponse.fromJSON(res.json() || {});
  }

  private extractArrayData(res: Response) {
    return res.json() || [];
  }

  private extractData(res: Response) {
    return res.json() || {};
  }

  private extractNoData(res: Response): boolean {
    if (res.ok && (res.text().length == 0 || res.json().success !== false)) {
      return true;
    } else {
      throw new Error("success=false");
    }
  }

  private handleError(error: Response | any) {
    // In a real world app, we might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      errMsg = `${error.status} - ${error.statusText || ''}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }
}
