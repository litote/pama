import {ChangeDetectorRef, Component, OnInit} from "@angular/core";
import {ActivatedRoute, Params, Router} from "@angular/router";
import {FileItem, FileUploader} from "ng2-file-upload";
import {AdminService} from "../admin.service";
import {Product} from "../../model/product";
import {DistributionType} from "../../model/distribution";
import {environment} from "../../../environments/environment";
import {MdDialog} from "@angular/material";
import {ConfirmDialogComponent} from "../../shared/confirm-dialog/confirm-dialog.component";

@Component({
  selector: 'app-product-edit',
  templateUrl: './product-edit.component.html',
  styleUrls: ['./product-edit.component.css']
})
export class ProductEditComponent implements OnInit {

  product: Product;
  uploader: FileUploader;
  loadingImage: boolean = false;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private adminService: AdminService,
              private ref: ChangeDetectorRef,
              private dialog: MdDialog) {
  }

  ngOnInit() {
    this.route.params.forEach((params: Params) => {
      const id = params['id'];
      if (id !== "new") {
        this.loadProduct(id);
      } else {
        this.product = new Product(null, null, DistributionType.Vegetables, null);
      }
    });
  }

  private loadProduct(id: string) {
    const this_ = this;
    this.adminService.getProduct(id).subscribe(p => {
        const uploader = new FileUploader({url: `${environment.serverUrl}/admin/product/uploadImage/${id}`});
        this_.uploader = uploader;
        uploader.onAfterAddingFile = function (i: FileItem) {
          this_.loadingImage = true;
          this_.ref.detectChanges();
          uploader.uploadItem(i);
        };
        uploader.onCompleteItem = function () {
          this_.loadProduct(id);
        };
        this_.product = p;
        this_.loadingImage = false;
      }, error => window.alert(error)
    );
  }

  createOrUpdateProduct() {
    this.adminService.saveProduct(this.product).subscribe(
      status => {
        this.cancel();
      },
      error => {
        window.alert(error)
      }
    );
    return false;
  }

  deleteProduct() {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title: "Supprimer le produit",
        subtitle: "Etes-vous sûr?",
        action: "Confirmer"
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result === "confirm") {
        this.adminService.deleteProduct(this.product).subscribe(
          status => {
            this.cancel();
          },
          error => {
            window.alert(error)
          }
        );
      }
    });
    return false;
  }

  cancel(): boolean {
    this.router.navigateByUrl("/admin?tab=products");
    return false;
  }

}
