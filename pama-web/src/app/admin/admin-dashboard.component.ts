import {Component, OnInit} from "@angular/core";
import {Cooperator} from "../model/cooperator";
import {AdminService} from "./admin.service";
import {ActivatedRoute, Params} from "@angular/router";
import {Distribution} from "../model/distribution";
import {DistributionService} from "../core/distribution.service";
import {Product} from "../model/product";

@Component({
  selector: 'admin-dashboard',
  templateUrl: './admin-dashboard.component.html',
  styleUrls: ['./admin-dashboard.component.css']
})
export class AdminDashboardComponent implements OnInit {
  cooperators: Cooperator[];
  distributions: Distribution[];
  products: Product[];
  selectedTab: number;
  displayOldDistributions: boolean = false;

  constructor(private route: ActivatedRoute,
              private adminService: AdminService,
              private distributionService: DistributionService) {
  }

  ngOnInit() {
    this.route.queryParams.forEach((params: Params) => {
      let tab = params['tab'];
      if (!tab) {
        tab = "0";
      } else if (tab == "cooperators") {
        tab = "1";
      } else if (tab == "products") {
        tab = "2";
      } else {
        tab = "0";
      }
      this.selectedTab = +tab;
      this.loadTab();
    });
  }

  private loadTab() {
    switch (this.selectedTab) {
      case 1:
        if (!this.cooperators) this.adminService.getCooperators().subscribe(
          cooperators => this.cooperators = cooperators,
          error => window.alert(error)
        );
        break;
      case 2:
        if (!this.products) this.adminService.getProducts().subscribe(
          products => this.products = products,
          error => window.alert(error)
        );
        break;
      default:
        if (!this.distributions)
        //TODO admin component
          this.distributionService.getDistributionCalendar().subscribe(
            distributions => this.distributions = distributions.distributions,
            error => window.alert(error)
          );
    }
  }

  onTabChange(tab: number) {
    this.selectedTab = tab;
    this.loadTab();
  }

}
