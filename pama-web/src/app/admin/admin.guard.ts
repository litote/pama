import {Injectable} from "@angular/core";
import {CanActivate, CanActivateChild, ActivatedRouteSnapshot, RouterStateSnapshot} from "@angular/router";
import {LoggedCooperator} from "../core/logged-cooperator";

@Injectable()
export class AdminGuard implements CanActivate, CanActivateChild {

  constructor(private loggedCooperator: LoggedCooperator) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.loggedCooperator.isAdmin();
  }

  canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    return this.canActivate(route, state);
  }

}
