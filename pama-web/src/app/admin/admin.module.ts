import {NgModule} from "@angular/core";
import {DistributionComponent} from "./distribution.component";
import {UserEditComponent} from "./user-edit.component";
import {AdminService} from "./admin.service";
import {SharedModule} from "../shared/shared.module";
import {AdminRoutingModule} from "./admin-routing.module";
import {AdminDashboardComponent} from "./admin-dashboard.component";
import {SelectModule} from "angular2-select";
import {AdminGuard} from "./admin.guard";
import {AlertsComponent} from "./alerts.component";
import {MyDatePickerModule} from "mydatepicker";
import { ProductEditComponent } from './product-edit/product-edit.component';
import {FileUploadModule} from "ng2-file-upload";

@NgModule({
  imports: [
    SharedModule,
    AdminRoutingModule,
    SelectModule,
    MyDatePickerModule,
    FileUploadModule
  ],
  declarations: [
    AdminDashboardComponent,
    UserEditComponent,
    DistributionComponent,
    AlertsComponent,
    ProductEditComponent
  ],
  providers: [
    AdminGuard,
    AdminService
  ]
})
export class AdminModule {
}
