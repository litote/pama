/*
 *     PAMA
 *     Copyright (C) 2016  Litote
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as
 *     published by the Free Software Foundation, either version 3 of the
 *     License, or (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
import {Component, OnInit} from "@angular/core";
import {AdminService} from "./admin.service";
import {LoggedCooperator} from "../core/logged-cooperator";
import {
  Alert,
  callForVolunteersAlertType,
  definePasswordAlertType,
  nextDistributionAlertType,
  TestAlertRequest
} from "../model/alerts";
import {Observable} from "rxjs";
@Component({
  selector: 'alerts',
  templateUrl: './alerts.component.html',
  styleUrls: ['./alerts.component.css']
})
export class AlertsComponent implements OnInit {

  sending: boolean;
  sent: boolean;
  error: boolean;
  definePasswordSent: boolean;

  nextDistributionAlert: Alert;
  callForVolunteersAlert: Alert;
  definePasswordAlert: Alert;

  lessparticipation: any[];

  constructor(private adminService: AdminService, private loggedCooperator: LoggedCooperator) {

  }

  ngOnInit(): void {
    this.adminService.getAlert(nextDistributionAlertType).subscribe(
      a => this.nextDistributionAlert = a
    );
    this.adminService.getAlert(callForVolunteersAlertType).subscribe(
      a => this.callForVolunteersAlert = a
    );
    this.adminService.getCooperatorsWithLessParticipation().subscribe(
      a => this.lessparticipation = a
    );
    this.adminService.getAlert(definePasswordAlertType).subscribe(
      a => this.definePasswordAlert = a
    );
  }

  saveAlert(alert: Alert) {
    this.send(
      alert,
      () => {
      },
      alert => Observable.of(true))
  }

  testDefinePasswordAlert() {
    this.test(this.definePasswordAlert);
  }

  sendDefinePasswordToAll() {
    const this_ = this;
    this.send(
      this.definePasswordAlert,
      () => this_.definePasswordSent = true,
      _ => this_.adminService.sendPasswordDefinitionAlert()
    );
  }


  testNextDistributionAlert() {
    this.test(this.nextDistributionAlert);
  }

  testCallForVolunteersAlert() {
    this.test(this.callForVolunteersAlert);
  }

  private test(alert: Alert) {
    const this_ = this;
    this.send(
      alert,
      () => this_.sent = true,
      alert => this_.adminService.testAlert(new TestAlertRequest(alert.alertType, this.loggedCooperator.cooperator._id))
    );
  }

  private send(alert: Alert, displayMessage: () => void, send: (alert: Alert) => Observable<boolean>) {
    this.sending = true;
    this.sent = false;
    this.error = false;
    const this_ = this;
    this.adminService
      .saveAlert(alert)
      .subscribe(
        b => send(alert)
          .subscribe(
            () => {
              this_.sending = false;
              displayMessage();
            },
            error => {
              this_.sending = false;
              this_.error = true;
            }
          ));
  }
}
