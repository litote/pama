import {Component, OnInit} from "@angular/core";
import {DistributionService} from "./core/distribution.service";
import {Distribution, DistributionCalendar} from "./model/distribution";
import {LoggedCooperator} from "./core/logged-cooperator";

@Component({
  selector: 'next-distributions',
  templateUrl: './next-distributions.component.html',
  styleUrls: ['./commons.css', './next-distributions.component.css']
})
export class NextDistributionsComponent implements OnInit {

  distributionCalendar: DistributionCalendar;
  distributions: Distribution[];
  nbCols: number;
  viewInThePast: boolean = false;

  constructor(public loggedCooperator: LoggedCooperator,
              public distributionService: DistributionService) {
  }

  ngOnInit() {
    this.distributionService.getDistributionCalendar().subscribe(
      distributionCalendar => this.init(distributionCalendar),
      error => window.alert(error));
  }

  private init(distributionCalendar: DistributionCalendar) {
    this.onResize(window.innerWidth);
    this.distributionCalendar = distributionCalendar;
    this.changeViewInThePast(false);
  }

  changeViewInThePast(inThePast: boolean) {
    this.viewInThePast = inThePast;
    this.distributions = this.distributionCalendar.distributions.filter(d => this.viewInThePast || !d.doNotDisplayBecauseIsInThePast());
  }

  onResize(innerWidth: number) {
    this.nbCols = Math.floor((innerWidth - (innerWidth % 128)) / 300);
  }

}
