/*
 *     PAMA
 *     Copyright (C) 2016  Litote
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as
 *     published by the Free Software Foundation, either version 3 of the
 *     License, or (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import {Component, Input, OnChanges, OnInit, SimpleChanges} from "@angular/core";
import {
  AllowedDistributionRole,
  Distribution,
  DistributionAttendance,
  DistributionRole,
  DistributionType
} from "../../model/distribution";
import {Cooperator, CooperatorInfo, CooperatorParticipation} from "../../model/cooperator";
import {LoggedCooperator} from "../../core/logged-cooperator";
import {MdDialog, MdSnackBar} from "@angular/material";
import {ChooseDistributionRoleDialogComponent} from "./choose-distribution-role-dialog.component";
import {ChooseDistributionNumberDialogComponent} from "./choose-distribution-number-dialog.component";
import {DistributionService} from "../../core/distribution.service";
import {ConfirmDialogComponent} from "../../shared/confirm-dialog/confirm-dialog.component";

@Component({
  selector: 'distribution-subscriber',
  templateUrl: './distribution-subscriber.component.html',
  styleUrls: ['./distribution-subscriber.component.css']
})
export class DistributionSubscriberComponent implements OnInit, OnChanges {

  @Input()
  distribution: Distribution;
  @Input()
  cooperators: CooperatorInfo[];
  @Input()
  distributionDetails: boolean;
  @Input()
  userDistributionMention: boolean = true;

  participate: boolean;
  participations: CooperatorParticipation[] = [];
  private participateChange: boolean;

  allCooperators: Cooperator[];
  selectedCooperator: Cooperator;

  constructor(public loggedCooperator: LoggedCooperator,
              private dialog: MdDialog,
              private distributionService: DistributionService,
              private snackBar: MdSnackBar) {
  }

  ngOnInit(): void {
    this.participate = this.loggedCooperator.participate(this.distribution._id);
    this.participateChange = this.participate;
    this.participations = this.loggedCooperator.participations.filter(p => p.present);
    if (this.loggedCooperator.isLogged() && this.loggedCooperator.isAdmin()) {
      this.distributionService.getCooperators().subscribe(all => this.allCooperators = all);
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.ngOnInit()
  }

  cooperatorInfo(cooperatorId: string): CooperatorInfo {
    const r = this.cooperators.find(c => c._id === cooperatorId);
    if (r) {
      return r;
    }
    if (cooperatorId === this.loggedCooperator.cooperator._id) {
      return new CooperatorInfo(cooperatorId, this.loggedCooperator.cooperator.firstName, this.loggedCooperator.cooperator.lastName);
    } else {
      return new CooperatorInfo("", "", "")
    }
  }

  toogleParticipate(value: boolean) {
    if (!this.participateChange) {
      if (this.loggedCooperator.isLogged()) {
        const c = this.loggedCooperator.cooperator;
        if (value && this.distribution.couldHaveReferentWith(c)) {
          const choices = this.distribution.allowedDistributionRoles(c);
          const dialogRef = this.dialog.open(ChooseDistributionRoleDialogComponent, {data: {choices: choices}});
          dialogRef.afterClosed().subscribe(result => {
            if (result === "confirm") {
              this.updateParticipation(value, dialogRef.componentInstance.selectedChoice);
            } else {
              this.participateChange = true;
              this.participate = !value;
            }
          });
        } else {
          this.updateParticipation(value);
        }
      }
    } else {
      this.participateChange = false;
    }
  }

  toogleCooperatorParticipate(value: boolean, cooperatorId?: string) {
    const cId = cooperatorId ? cooperatorId : this.selectedCooperator._id;
    this.distributionService.postParticipation(new CooperatorParticipation(
      cId,
      this.distribution._id,
      DistributionRole.Normal,
      1,
      value)).subscribe(_ => {
        if(value) {
          this.cooperators.push(this.allCooperators.filter(c => c._id === cId)[0].toCooperatorInfo());
          this.distribution.attendance.push(new DistributionAttendance(
            DistributionType.Vegetables,
            DistributionRole.Normal,
            cId,
            true));
        } else {
          this.distribution.attendance = this.distribution.attendance.filter(c => c.cooperatorId !== cId);
        }
      this.snackBar.open("Modification prise en compte", "Inscription", {duration: 1000})
    });
  }

  deleteParticipate(cooperatorId: string) {
    const c = this.allCooperators.filter(c => c._id === cooperatorId)[0];
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title: "Désinscrire l'utilisateur "+c.firstName+" "+c.lastName,
        subtitle: "Etes-vous sûr?",
        action: "Confirmer"
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result === "confirm") {
        this.toogleCooperatorParticipate(false, cooperatorId),
          error => {
            window.alert(error)
          }
      }
    });
  }

  private updateParticipation(value: boolean, role?: AllowedDistributionRole) {
    if (value
      && this.loggedCooperator.cooperator.additionalCooperators !== 0
      && (!role || role.role === DistributionRole.Normal)) {
      const choices = [1, 2];
      if (this.loggedCooperator.cooperator.additionalCooperators === 2) {
        choices.push(3);
      }
      const dialogRef = this.dialog.open(ChooseDistributionNumberDialogComponent, {data: {choices: choices}});
      dialogRef.afterClosed().subscribe(result => {
        if (result === "confirm") {
          this.updateParticipations(value, dialogRef.componentInstance.selectedChoice, role);
        } else {
          this.participateChange = true;
          this.participate = !value;
        }
      });
    } else {
      this.updateParticipations(value, 1, role);
    }
  }

  private updateParticipations(value: boolean, nbCooperators: number, role?: AllowedDistributionRole) {
    //force refresh trigger
    if (value) this.cooperators.push(this.loggedCooperator.cooperator.toCooperatorInfo());

    this.distribution.updateAttendance(this.loggedCooperator.cooperator, value, nbCooperators, role);
    this.loggedCooperator.addParticipation(
      new CooperatorParticipation(
        this.loggedCooperator.cooperator._id,
        this.distribution._id, role ? role.role : DistributionRole.Normal,
        nbCooperators,
        value))
      .subscribe(
        status => this.participations = this.loggedCooperator.participations.filter(p => p.present),
        error => window.alert(error)
      );
  }

  updatePresence(p: CooperatorParticipation, present: boolean) {
    p.distributionId = this.distribution._id;
    p.present = present;
    this.distributionService.updateParticipation(p).subscribe(b => this.snackBar.open("Modification prise en compte", "Présence", {duration: 1000}));
  }
}
