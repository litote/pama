/*
 *     PAMA
 *     Copyright (C) 2016  Litote
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as
 *     published by the Free Software Foundation, either version 3 of the
 *     License, or (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import {Component} from "@angular/core";
import {MdDialogRef} from "@angular/material";
import {AllowedDistributionRole} from "../../model/distribution";
@Component({
  selector: 'choose-distribution-role-dialog',
  templateUrl: './choose-distribution-role-dialog.component.html',
  styleUrls: ['./choose-distribution-role-dialog.component.css']
})
export class ChooseDistributionRoleDialogComponent {

  choices:AllowedDistributionRole[];
  selectedChoice:AllowedDistributionRole;

  constructor(public dialogRef: MdDialogRef<ChooseDistributionRoleDialogComponent>) {
      this.choices = dialogRef._containerInstance.dialogConfig.data.choices;
      this.selectedChoice = this.choices[0];
  }


}
