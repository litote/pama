import {Component, Input, Output, EventEmitter} from "@angular/core";
import {CooperatorInfo} from "../../model/cooperator";
@Component({
  selector: 'cooperator-contact',
  templateUrl: './cooperator-contact.component.html',
  styleUrls: ['./cooperator-contact.component.css']
})
export class CooperatorContactComponent {

  @Input()
  cooperator:CooperatorInfo;
  @Output()
  hideContact = new EventEmitter<boolean>();

  hide() {
    this.hideContact.emit(true);
  }
}
