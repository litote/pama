import {Component, Input} from "@angular/core";
import {Distribution, DistributionType, distributionTypeLabel} from "../../model/distribution";
import {CooperatorInfo} from "../../model/cooperator";
import {LoggedCooperator} from "../../core/logged-cooperator";

@Component({
  selector: 'distribution-types',
  templateUrl: './distribution-types.component.html',
  styleUrls: ['./distribution-types.component.css']
})
export class DistributionTypesComponent {

  @Input() distribution: Distribution;
  @Input() cooperators: CooperatorInfo[];
  @Input() details: boolean = true;

  constructor(public loggedCooperator: LoggedCooperator) {
  }

  displayDistributionType(type: DistributionType): string {
    return distributionTypeLabel(type);
  }

  referents(type: DistributionType): CooperatorInfo[] {
    const referentIds = this.distribution.getReferentIds(type);
    return this.cooperators.filter(c => referentIds.indexOf(c._id) !== -1);
  }

  coordinator(): CooperatorInfo {
    const coordinatorId = this.distribution.getCoordinatorId();
    return coordinatorId ? this.cooperators.filter(c => coordinatorId === c._id)[0] : null;
  }

}
