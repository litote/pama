import {Component, OnInit} from "@angular/core";
import {DistributionDetails} from "./model/distribution";
import {ActivatedRoute, Params} from "@angular/router";
import {DistributionService} from "./core/distribution.service";
import {Product} from "./model/product";
import {LoggedCooperator} from "./core/logged-cooperator";

@Component({
  selector: 'distribution-details',
  templateUrl: './distribution-details.component.html',
  styleUrls: ['./commons.css', './distribution-details.component.css']
})
export class DistributionDetailsComponent implements OnInit {

  distribution: DistributionDetails;
  selectedProduct: Product;

  constructor(public loggedCooperator: LoggedCooperator,
              private route: ActivatedRoute,
              private distributionService: DistributionService) {
  }

  ngOnInit() {
    this.route.params.forEach((params: Params) => {
      const id = params['id'];

      this.distributionService.getDistributionDetails(id).subscribe(
        distribution => this.load(distribution),
        error => window.alert(error)
      );
    });
  }

  private load(distribution: DistributionDetails) {
    if (distribution.distribution.hasProduct()) {
      this.selectedProduct = distribution.distribution.products[0];
    } else {
      this.selectedProduct = null;
    }
    this.distribution = distribution;
  }

  selectProduct(value: Product) {
    this.selectedProduct = value;
  }

}
