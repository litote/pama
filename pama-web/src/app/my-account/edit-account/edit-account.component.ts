/*
 *     PAMA
 *     Copyright (C) 2016  Litote
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as
 *     published by the Free Software Foundation, either version 3 of the
 *     License, or (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import {Component, OnInit, ViewChild} from "@angular/core";
import {LoggedCooperator} from "../../core/logged-cooperator";
import {MdInputContainer} from "@angular/material";
import {Cooperator, SaveCooperatorRequest} from "../../model/cooperator";
import {Router} from "@angular/router";
import {MyAccountService} from "../my-account-service";

@Component({
  selector: 'edit-account',
  templateUrl: './edit-account.component.html',
  styleUrls: ['./edit-account.component.css']
})
export class EditAccountComponent implements OnInit {

  cooperator: Cooperator;
  saveError: string;

  @ViewChild('firstNameContainer') firstName: MdInputContainer;

  constructor(private router: Router,
              private myAccountService: MyAccountService,
              private loggedCooperator: LoggedCooperator) {
    this.cooperator = loggedCooperator.cooperator;
  }

  ngOnInit(): void {
    this.firstName._focusInput();
  }

  cancel(): boolean {
    this.router.navigateByUrl("/");
    return false;
  }

  onSubmit(): boolean {
    this.saveError = null;
    const request = new SaveCooperatorRequest(this.cooperator, []);
    this.myAccountService.saveCooperator(request).subscribe(
      response => {
        if (response.success) {
          this.router.navigateByUrl("/");
        } else {
          this.saveError = response.errorMessage;
        }
      },
      error => window.alert(error)
    );

    return false;
  }
}
