/*
 *     PAMA
 *     Copyright (C) 2016  Litote
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as
 *     published by the Free Software Foundation, either version 3 of the
 *     License, or (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import {Http, Response} from "@angular/http";
import {Injectable} from "@angular/core";
import {Cooperator, SaveCooperatorRequest, SaveCooperatorResponse} from "../model/cooperator";
import {Observable} from "rxjs/Observable";
import {environment} from "../../environments/environment";
@Injectable()
export class MyAccountService {

  constructor(private http: Http) {
  }

  private static extractSaveCooperatorResponse(res: Response): SaveCooperatorResponse {
    return SaveCooperatorResponse.fromJSON(res.json() || {});
  }

  saveCooperator(request: SaveCooperatorRequest): Observable<SaveCooperatorResponse> {
    return this.http.post(`${environment.serverUrl}/cooperator/${request.cooperator._id}`, JSON.stringify(request))
      .map(MyAccountService.extractSaveCooperatorResponse)
      .catch(this.handleError);
  }

  private extractNoData(res: Response): boolean {
    if (res.ok && (res.text().length == 0 || res.json().success !== false)) {
      return true;
    } else {
      throw new Error("success=false");
    }
  }

  private handleError(error: Response | any) {
    // In a real world app, we might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      errMsg = `${error.status} - ${error.statusText || ''}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }

}
