import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {FormsModule} from "@angular/forms";
import {MaterialRootModule} from "@angular/material";
import {MomentModule} from "angular2-moment";
import {DistributionTypesComponent} from "../distribution/distribution-types/distribution-types.component";
import {CooperatorContactComponent} from "../distribution/cooperator-contact/cooperator-contact.component";
import {ConfirmDialogComponent} from "./confirm-dialog/confirm-dialog.component";

@NgModule({
  imports: [CommonModule, MaterialRootModule], //TODO remove Material
  declarations: [DistributionTypesComponent, CooperatorContactComponent, ConfirmDialogComponent], //TODO move DistributionTypesComponent,CooperatorContactComponent
  exports: [CommonModule, FormsModule, MaterialRootModule, MomentModule, DistributionTypesComponent, CooperatorContactComponent],
  entryComponents: [
    ConfirmDialogComponent
  ]
})
export class SharedModule {
}
