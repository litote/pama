import {BrowserModule} from "@angular/platform-browser";
import {NgModule} from "@angular/core";
import {AppRoutingModule} from "./app-routing.module";
import {AppComponent} from "./app.component";
import {HomeComponent} from "./home.component";
import {DistributionDetailsComponent} from "./distribution-details.component";
import "hammerjs";
import "moment/locale/fr";
import {MasonryModule} from "angular2-masonry/src/module";
import {NextDistributionsComponent} from "./next-distributions.component";
import {SharedModule} from "./shared/shared.module";
import {CoreModule} from "./core/core.module";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {DistributionSubscriberComponent} from "./distribution/distribution-subscriber/distribution-subscriber.component";
import {ChooseDistributionRoleDialogComponent} from "./distribution/distribution-subscriber/choose-distribution-role-dialog.component";
import {ChooseDistributionNumberDialogComponent} from "./distribution/distribution-subscriber/choose-distribution-number-dialog.component";

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    DistributionDetailsComponent,
    NextDistributionsComponent,
    DistributionSubscriberComponent,
    ChooseDistributionRoleDialogComponent,
    ChooseDistributionNumberDialogComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    SharedModule,
    CoreModule,
    AppRoutingModule,
    MasonryModule
  ],
  providers: [],
  entryComponents: [
    ChooseDistributionRoleDialogComponent,
    ChooseDistributionNumberDialogComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
